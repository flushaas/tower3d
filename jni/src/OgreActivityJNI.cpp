#include "OgrePlatform.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
#include "towerCommon.h"
#include <jni.h>
#include <EGL/egl.h>
#include <android/api-level.h>
#include <android/native_window_jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include "OgrePlatform.h"
#include "OgreRoot.h"
#include "OgreRenderWindow.h"
#include "OgreArchiveManager.h"
#include "OgreMovableObject.h"
#include "OgreFileSystemLayer.h"
#include "OgreEntity.h"
#include "OgreMeshManager.h"
#include "Android/OgreAndroidEGLWindow.h"
#include "Android/OgreAPKFileSystemArchive.h"
#include "Android/OgreAPKZipArchive.h"
#include "OgreParticleSystemManager.h"
#include <android/log.h>
#include <android/asset_manager.h>
#include <OgreShaderGenerator.h>
#include "GameManager.h"
#include "PlayState.h"
#include "SDL.h"

#define LOGI(...) \
  ((void)__android_log_print(ANDROID_LOG_INFO, "Ogre", __VA_ARGS__))
#define LOGW(...) \
  ((void)__android_log_print(ANDROID_LOG_WARN, "Ogre", __VA_ARGS__))

#ifdef OGRE_BUILD_PLUGIN_OCTREE
#include "OgreOctreePlugin.h"
#endif

//#ifdef OGRE_BUILD_PLUGIN_PFX
#include "OgreParticleFXPlugin.h"
//#endif
#ifdef OGRE_BUILD_COMPONENT_OVERLAY
#include "OgreOverlaySystem.h"
#endif

#include "OgreConfigFile.h"

#ifdef OGRE_BUILD_RENDERSYSTEM_GLES2
#include "OgreGLES2Plugin.h"
#define GLESRS GLES2Plugin
#else
#include "OgreGLESPlugin.h"
#define GLESRS GLESPlugin
#endif

using namespace Ogre;

class ShaderGeneratorTechniqueResolverListener
    : public Ogre::MaterialManager::Listener {
 public:
  ShaderGeneratorTechniqueResolverListener(
      Ogre::RTShader::ShaderGenerator* pShaderGenerator) {
    mShaderGenerator = pShaderGenerator;
  }

  /** This is the hook point where shader based technique will be created.
  It will be called whenever the material manager won't find appropriate
  technique
  that satisfy the target scheme name. If the scheme name is out target RT
  Shader System
  scheme name we will try to create shader generated technique for it.
  */
  virtual Ogre::Technique* handleSchemeNotFound(
      unsigned short schemeIndex, const Ogre::String& schemeName,
      Ogre::Material* originalMaterial, unsigned short lodIndex,
      const Ogre::Renderable* rend) {
    Ogre::Technique* generatedTech = NULL;

    // Case this is the default shader generator scheme.
    if (schemeName == Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME) {
      bool techniqueCreated;

      // Create shader generated technique for this material.
      techniqueCreated = mShaderGenerator->createShaderBasedTechnique(
          originalMaterial->getName(),
          Ogre::MaterialManager::DEFAULT_SCHEME_NAME, schemeName);

      // Case technique registration succeeded.
      if (techniqueCreated) {
        // Force creating the shaders for the generated technique.
        mShaderGenerator->validateMaterial(schemeName,
                                           originalMaterial->getName());

        // Grab the generated technique.
        Ogre::Material::TechniqueIterator itTech =
            originalMaterial->getTechniqueIterator();

        while (itTech.hasMoreElements()) {
          Ogre::Technique* curTech = itTech.getNext();

          if (curTech->getSchemeName() == schemeName) {
            generatedTech = curTech;
            break;
          }
        }
      }
    }

    return generatedTech;
  }

 protected:
  Ogre::RTShader::ShaderGenerator*
      mShaderGenerator;  // The shader generator instance.
};

static bool gInit = false;
static Ogre::Root* gRoot = NULL;
static Ogre::RenderWindow* gRenderWnd = NULL;

#ifdef OGRE_BUILD_PLUGIN_OCTREE
static Ogre::OctreePlugin* gOctreePlugin = NULL;
#endif

//#ifdef OGRE_BUILD_PLUGIN_PFX
static Ogre::ParticleFXPlugin* gParticleFXPlugin = NULL;
//#endif

#ifdef OGRE_BUILD_COMPONENT_OVERLAY
static Ogre::OverlaySystem* gOverlaySystem = NULL;
#endif

static Ogre::GLESRS* gGLESPlugin = NULL;

static Ogre::SceneManager* pSceneMgr = NULL;
static Ogre::Camera* pCamera = NULL;
static JavaVM* gVM = NULL;


AAssetManager* assetMgr;
ShaderGeneratorTechniqueResolverListener* material_mgr_listener_ = NULL;
/*
class MyFrameListener: public Ogre::FrameListener{

  bool frameStarted (const FrameEvent &evt){
    LOGW("---FrameStarted---");
    float degrees  = evt.timeSinceLastFrame*5;
    LOGW("DEGREEES ");
    //LOGW(degrees);
    if(pCamera != NULL){
      pCamera->yaw(Ogre::Radian(Ogre::Degree(degrees)));
      LOGW("CAMERA NO NULA");
    }
    else{
      LOGW("camera nula");
    }
  }
  bool frameEnded(const FrameEvent &evt){
    LOGW("---FrameStarted---");
  }
  };*/
extern "C"

    {
/**
 * Open APK file to access resources in Android
 *

Ogre::DataStreamPtr openAPKFile(const Ogre::String& fileName) {
  Ogre::DataStreamPtr stream;
  AAsset* asset =
      AAssetManager_open(assetMgr, fileName.c_str(), AASSET_MODE_BUFFER);
  if (asset) {
    off_t length = AAsset_getLength(asset);
    void* membuf = OGRE_MALLOC(length, Ogre::MEMCATEGORY_GENERAL);
    memcpy(membuf, AAsset_getBuffer(asset), length);
    AAsset_close(asset);

    stream = Ogre::DataStreamPtr(
        new Ogre::MemoryDataStream(membuf, length, true, true));
  }
  return stream;
}
*/

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved) {
  gVM = vm;
  return JNI_VERSION_1_4;
}

/**
 *
 * Call when created the Android Activity.
 * Initializes:
 * - Ogre Root
 * - OctreePlugin
 * - Particles FX
 * - Overlay
 * - RenderSystem
 * - Resources With AAsetManager
 *
 */
JNIEXPORT void JNICALL Java_org_ogre3d_android_OgreActivityJNI_create(
    JNIEnv* env, jobject obj, jobject assetManager) {
    assetMgr = AAssetManager_fromJava(env, assetManager);
    

    /*  gRoot = new Ogre::Root();

  gGLESPlugin = OGRE_NEW GLESRS();
  gRoot->installPlugin(gGLESPlugin);

#ifdef OGRE_BUILD_PLUGIN_OCTREE
  gOctreePlugin = OGRE_NEW OctreePlugin();
  gRoot->installPlugin(gOctreePlugin);
#endif

  //#ifdef OGRE_BUILD_PLUGIN_PFX
  gParticleFXPlugin = OGRE_NEW ParticleFXPlugin();
  gRoot->installPlugin(gParticleFXPlugin);
  //#endif

#ifdef OGRE_BUILD_COMPONENT_OVERLAY
  gOverlaySystem = OGRE_NEW OverlaySystem();
#endif

  gRoot->setRenderSystem(gRoot->getAvailableRenderers().at(0));

  gRoot->initialise(false);
  
  gInit = true;

    */


}

JNIEXPORT void JNICALL
Java_org_ogre3d_android_OgreActivityJNI_destroy(JNIEnv* env, jobject obj) {
  if (!gInit) return;

  gInit = false;

#ifdef OGRE_BUILD_COMPONENT_OVERLAY
  OGRE_DELETE gOverlaySystem;
  gOverlaySystem = NULL;
#endif

  OGRE_DELETE gRoot;
  gRoot = NULL;
  gRenderWnd = NULL;

  //#ifdef OGRE_BUILD_PLUGIN_PFX
  OGRE_DELETE gParticleFXPlugin;
  gParticleFXPlugin = NULL;
  //#endif

#ifdef OGRE_BUILD_PLUGIN_OCTREE
  OGRE_DELETE gOctreePlugin;
  gOctreePlugin = NULL;
#endif

  OGRE_DELETE gGLESPlugin;
  gGLESPlugin = NULL;
}

JNIEXPORT void JNICALL Java_org_ogre3d_android_OgreActivityJNI_initWindow(
    JNIEnv* env, jobject obj, jobject surface) {
  LOGW(
      "3****************************************\n*****************************"
      "**\n**********************\n****************");

      SDL_Init(SDL_INIT_VIDEO);
SDL_Window *screen = SDL_CreateWindow("My Game Window",
                          SDL_WINDOWPOS_UNDEFINED,
                          SDL_WINDOWPOS_UNDEFINED,
                          800, 600,
                          SDL_WINDOW_OPENGL);

  if (surface) {
    ANativeWindow* nativeWnd = ANativeWindow_fromSurface(env, surface);
    if (nativeWnd && gRoot) {
      if (!gRenderWnd) {
        LOGW(
            "4****************************************\n***********************"
            "********\n**********************\n****************");
        Ogre::NameValuePairList opt;
        opt["externalWindowHandle"] =
            Ogre::StringConverter::toString((int)nativeWnd);
        gRenderWnd = Ogre::Root::getSingleton().createRenderWindow(
            "OgreWindow", 0, 0, false, &opt);

        if (pSceneMgr == NULL) {
          pSceneMgr = gRoot->createSceneManager(Ogre::ST_GENERIC);
          LOGW("##################\n\n\n\n\n\n\n\n\n\n\n\n\n\n#MUERTE!!\n ");
          // Setup shaders
          Ogre::RTShader::ShaderGenerator::initialize();

          // The Shader generator instance
          Ogre::RTShader::ShaderGenerator* gen =
              Ogre::RTShader::ShaderGenerator::getSingletonPtr();

          // Create and register the material manager listener if it doesn't
          // exist yet.
          if (material_mgr_listener_ == NULL) {
            material_mgr_listener_ =
                new ShaderGeneratorTechniqueResolverListener(gen);
            Ogre::MaterialManager::getSingleton().addListener(
                material_mgr_listener_);
          }

          gen->addSceneManager(pSceneMgr);

          GameManager* gameMgr = new GameManager();
	  //          gameMgr->androidStart(new Tower3d::PlayState());
        }
      } else {
        static_cast<Ogre::AndroidEGLWindow*>(gRenderWnd)
            ->_createInternalResources(nativeWnd, NULL);
      }


  if (assetMgr) {
    ArchiveManager::getSingleton().addArchiveFactory(
        new APKFileSystemArchiveFactory(assetMgr));
    ArchiveManager::getSingleton().addArchiveFactory(
        new APKZipArchiveFactory(assetMgr));
  }

  Ogre::FileSystemLayer* mFSLayer = OGRE_NEW_T(
      Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)(OGRE_VERSION_NAME);
  Ogre::ConfigFile cfg;

  //  cfg.load(openAPKFile(mFSLayer->getConfigFilePath("resources.cfg")));

  Ogre::ResourceGroupManager& resGroupMan =
      Ogre::ResourceGroupManager::getSingleton();
  Ogre::String defResGroup =
      Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;

  resGroupMan.addResourceLocation("/", "APKFileSystem", defResGroup);
  resGroupMan.addResourceLocation("/maps", "APKFileSystem", defResGroup);
  resGroupMan.addResourceLocation("/tiles", "APKFileSystem", defResGroup);
  resGroupMan.addResourceLocation("/material", "APKFileSystem", defResGroup);
    resGroupMan.addResourceLocation("/overlays", "APKFileSystem", defResGroup);
      resGroupMan.addResourceLocation("/towers", "APKFileSystem", defResGroup);
        resGroupMan.addResourceLocation("/soldiers", "APKFileSystem", defResGroup);
  resGroupMan.initialiseAllResourceGroups();
    }
  }
}

/**
 * Called when apps ended. Destroy resources
 *
 */
JNIEXPORT void JNICALL
Java_org_ogre3d_android_OgreActivityJNI_termWindow(JNIEnv* env, jobject obj) {
  if (gRoot && gRenderWnd) {
    static_cast<Ogre::AndroidEGLWindow*>(gRenderWnd)
        ->_destroyInternalResources();
  }
}

/**
 * Render one Frame . Calls Ogre Root to render frame by frame
 */
JNIEXPORT void JNICALL Java_org_ogre3d_android_OgreActivityJNI_renderOneFrame(
    JNIEnv* env, jobject obj) {
  if (gRenderWnd != NULL && gRenderWnd->isActive()) {
    try {
      if (gVM->AttachCurrentThread(&env, NULL) < 0) return;

      gRenderWnd->windowMovedOrResized();
      gRoot->renderOneFrame();

      // gVM->DetachCurrentThread();
    } catch (Ogre::RenderingAPIException ex) {
    }
  }
}
};

#endif

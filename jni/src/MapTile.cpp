#include "MapTile.h"
#include "OgreSceneManager.h"
#include "OgreEntity.h"
#include "OgreRoot.h"
#include "towerCommon.h"
#include "TileType.h"

namespace Tower3d {
/**
 * Draws this tile
 */
void MapTile::draw() {
  Ogre::SceneManager *scnMgr =
      Ogre::Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);

  _node = scnMgr->createSceneNode(_id);
  /* Ogre::Entity *ent;

  if (getType() == TileType::TERRAIN) {
    ent = scnMgr->createEntity(_id,"terrainTile.mesh");
  } else {
    ent = scnMgr->createEntity(_id,"roadTile.mesh");
  }
  ent->setCastShadows(false);
  ent->setQueryFlags(2);
  _node->attachObject(ent);
  */
  scnMgr->getSceneNode(MAP_SCENE_NODE)->addChild(_node);
  _node->setPosition(getPosition());
}
}

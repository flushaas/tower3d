#include "WaveManager.h"
#include "OgreConfigFile.h"
#include "OgreFileSystemLayer.h"

#include <ctime>

Ogre::DataStreamPtr openAPKFile(const Ogre::String &fileName);
namespace Tower3d {
/**
 * WaveManager Manage the waves of enemys. Parse waves.cfg
 */
WaveManager::WaveManager(std::shared_ptr<EnemyFactory> enemyFactory) {

  long nextWaveTime = static_cast<long>(time(NULL)) + (long)TIME_OFFSET_END;

  _enemyFactory = enemyFactory;
  _timeSinceLastWave = -1;

  _currentWaveIndex = -1;

  Ogre::ConfigFile cf;
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
  Ogre::FileSystemLayer *mFSLayer = OGRE_NEW_T(
      Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)(OGRE_VERSION_NAME);

  cf.load(openAPKFile(mFSLayer->getConfigFilePath("waves.cfg")));
#endif
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
  cf.load("../assets/waves.cfg");
#endif

  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  while (sI.hasMoreElements()) {

    std::string idWave = sI.peekNextKey();

    t_waveProperties waveProperties;
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      waveProperties.insert(
          std::pair<std::string, std::string>(i->first, i->second));
    }
    if (!idWave.empty()) {
      stringstream str;
      str<< _waves.size();
      waveProperties.insert(std::pair<std::string,std::string>("idWave",str.str()));
      std::shared_ptr<Wave> wave = std::shared_ptr<Wave>(new Wave(waveProperties, _enemyFactory, nextWaveTime));
      _waves.push_back(wave);
      nextWaveTime = wave->getAproxEndTime();
    }
  }
}

std::vector<std::shared_ptr<Enemy>> WaveManager::getActiveEnemies() {
  std::vector<std::shared_ptr<Enemy>> activeEnemies;
  for (uint i = 0; i < _waves.size(); i++) {
    if (_waves[i]->isActive()) {
      std::vector<std::shared_ptr<Enemy>> activeWaveEnemies =
          _waves[i].get()->getActiveEnemies();
      activeEnemies.insert(activeEnemies.end(), activeWaveEnemies.begin(),
                           activeWaveEnemies.end());
    }
  }

  return activeEnemies;
}

bool  WaveManager::isEnded() {

  bool ended=false;
  int nWavesActivas=0;
  for (uint i = 0; i < _waves.size(); i++) {
    if (_waves[i]->isActive()) {
      nWavesActivas++;

    }
  }

  if(nWavesActivas == _waves.size()){
    ended = getActiveEnemies().size() == 0;
  }
  return ended;
}

int WaveManager::updateWaves(double deltaTime) {

  long currentTime = (long)time(NULL);
  int livesTaken = 0;

  // Check if we have to Start a new Wave
  /*  if (_timeSinceLastWave > WAVE_INTERVAL) {
    _timeSinceLastWave = 0;

    if (_currentWaveIndex < ((int)_waves.size() - 1)) {
      _currentWaveIndex++;
    } else {
    }
    }*/
  for (uint i = 0; i < _waves.size(); i++) {
    if (_waves[i]->isActive()) {
      _currentWaveIndex = i;
      livesTaken += _waves[i].get()->update(deltaTime);
    }
  }
  return livesTaken;
}

Reward
WaveManager::manageHits(std::vector<std::shared_ptr<Projectile>> projectiles) {

  Reward reward;
  reward.money = 0;
  reward.points = 0;

  for (uint i = 0; i < _waves.size(); i++) {
    if (_waves[i]->isActive()) {

      Reward waveReward = _waves[i].get()->manageHits(projectiles);
      reward.money += waveReward.money;
      reward.points += waveReward.points;
    }
  }
  return reward;
}
}

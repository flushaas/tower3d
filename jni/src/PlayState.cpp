#include "OgrePlatform.h"
#include <iostream>
#include "PlayState.h"
#include "towerCommon.h"
#include "OgreRoot.h"
#include "OgreCamera.h"
#include "OgreSceneManager.h"
#include "OgreLight.h"
#include "OgreEntity.h"
#include "Map.h"
#include "MapManager.h"
#include "OgreResourceGroupManager.h"
#include "OgreOverlayManager.h"
#include "OgreOverlayContainer.h"
#include "OgreOverlayElement.h"
#include "EndGameState.h"

#include "OgreRenderTarget.h"

#include "OgreUtil.h"
#include <boost/algorithm/string/predicate.hpp>
#define TOWER_ENDING "Tower"
#define NOT_ENOUGH_MONEY "Don't have enough money"

using namespace Ogre;

namespace Tower3d {

/**
 * Play State get current scene Manager, create Main Camera, and add ViewPort
 */
void PlayState::enter() {
  _root = Root::getSingletonPtr();

  _sceneMgr = _root->getSceneManager(MAIN_SCN_MGR);

  _camera = _sceneMgr->getCamera("MainCamera");

  createScene();
  createHUD();

  _towerFactory = std::unique_ptr<TowerFactory>(new TowerFactory());
  _enemyFactory = std::shared_ptr<EnemyFactory>(new EnemyFactory(_map));
  _waveManager = std::unique_ptr<WaveManager>(new WaveManager(_enemyFactory));

  _deltaT = 0;
  _numTowers = 0;
}

/**
 * clear Scene and remove ViewPorts
 */
void PlayState::exit() {
  // sounds::getInstance()->halt_music();
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  mg->getByName("Tower3d/playOverlay")->hide();
  _selectedTower =NULL;
  _buildingTower = NULL;



  hideTowerInfo();
  
   _sceneMgr->clearScene();
}

/**
 * Nothing to do with pause yet
 */
void PlayState::pause() {}

/**
 * Nothing to do with resume
 */
void PlayState::resume() {}

void PlayState::updateHUDInfo() {
  std::string win = "OgreWindow";
  Ogre::RenderTarget::FrameStats fs =
      ((Ogre::RenderWindow *)Ogre::Root::getSingleton().getRenderTarget(win))
          ->getStatistics();
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  stringstream str;
  str << _lives;
  // str << fs.avgFPS;
  mg->getOverlayElement("text/textLives")->setCaption(str.str());
  str.str("");
  str.clear();
  str << _money;
  // str << fs.lastFPS;
  mg->getOverlayElement("text/MoneyText")->setCaption(str.str());
  str.str("");
  str.clear();
  str << _score;
  // str << fs.triangleCount;

  mg->getOverlayElement("text/ScoreText")->setCaption(str.str());

  str.str("");
  str.clear();
  str << _waveManager->getCurrentWave()+1<<"/"<<_waveManager->getNumWaves() << "  "<< _waveManager->getSecondsToNextWave();
  mg->getOverlayElement("text/WavesValue")->setCaption(str.str());

}

/**
 * record deltaTime in local variable
 */

bool PlayState::frameStarted(const FrameEvent &evt) {

  _deltaT = evt.timeSinceLastFrame;
  if (_timeSinceLastMessage >= 0) {
    _timeSinceLastMessage += _deltaT;
    if (_timeSinceLastMessage > TIME_SHOWING_MESSAGE) {
      _timeSinceLastMessage = -1;
      OverlayManager *mg = OverlayManager::getSingletonPtr();
      mg->getByName("Tower3d/messageOverlay")->hide();
    }
  }

  _lives -= _waveManager->updateWaves(_deltaT);



  std::vector<std::shared_ptr<Enemy>> enemies =
      _waveManager->getActiveEnemies();

  std::for_each(
      enemies.begin(), enemies.end(), [this](std::shared_ptr<Enemy> enemy) {

        std::for_each(this->_towers.begin(), this->_towers.end(),
                      [enemy, this](std::shared_ptr<Tower> tower) {

                        tower.get()->checkEnemyOnRange(enemy, this->_deltaT);
                      });
      });

  checkTowersHits(_deltaT);
  updateHUDInfo();
  return true;
}

/**
 * Nothing to do yet
 */
bool PlayState::frameEnded(const FrameEvent &evt) {
  if(!_keepLiving){
    popState();

  }

  if(_lives<=0 && _keepLiving){

    pushState( new Tower3d::EndGameState(false,_score));
    _keepLiving = false;

  }
  else if (_waveManager->isEnded() && _keepLiving){
    pushState( new Tower3d::EndGameState(true,_score));
    _keepLiving = false;
  }

  return true; }
  

/**
 * Nothing to do yet
 */

bool PlayState::keyPressed(const SDL_Event *e) {

  if (e->key.keysym.sym == SDLK_ESCAPE) {
    _keepLiving = false;
  }
  else{
    _lives=0;

  }

  return true;
}

bool PlayState::keyReleased(const SDL_Event *e) {
  if (e->key.keysym.sym == SDLK_ESCAPE) {
    _keepLiving = false;
  }
  return true;
}

void PlayState::moveTower(Vector2 pos) {

  Ray mouseRay = _camera->getCameraToViewportRay(pos.x, pos.y);
  _rayScnQuery = _sceneMgr->createRayQuery(mouseRay, 0);
  _rayScnQuery->setRay(mouseRay);
  _rayScnQuery->setSortByDistance(true);
  _rayScnQuery->setQueryMask(2);
  RaySceneQueryResult &result = _rayScnQuery->execute();
  RaySceneQueryResult::iterator it = result.begin();

  for (int i = 0; it != result.end(); it++) {
    float distance = it->distance;
    if (it->movable) {

      // Vector3 tilePosition =
      // it->movable->getParentSceneNode()->getPosition();
      Vector3 tilePosition = mouseRay.getPoint(distance);

      _buildingTower->setPosition(tilePosition);
      _buildingTower->setValidPosition(checkValidNewTowerPosition());
    }
  }
  _sceneMgr->destroyQuery(_rayScnQuery);
}

bool PlayState::checkValidNewTowerPosition() {
  int towerCollisions = 0;

  AxisAlignedBox box = _buildingTower.get()->getNode()->_getWorldAABB();

  box.setMinimumY(-0.2);

  AxisAlignedBoxSceneQuery *aaquery = _sceneMgr->createAABBQuery(box);

  SceneQueryResult &queryResult = aaquery->execute();

  towerCollisions = std::count_if(
      queryResult.movables.begin(), queryResult.movables.end(),
      [=](Ogre::MovableObject *mObject) {

        if (mObject->getName().find("Tower") != std::string::npos &&
            mObject != _buildingTower.get()->getEntity()) {
          return true;
        } else if (mObject->getName().find("road") != std::string::npos) {
          return true;
        }
        return false;

      });


  return !_map->containsRoad(box) && towerCollisions == 0 ; 
}
bool PlayState::mouseMoved(const SDL_Event *e)

{
  if (_buildingTower) {

    moveTower(OgreUtil::getCursorPosition(e));
  }

  return true;
}

void PlayState::checkTowersHits(float deltaTime) {

  std::vector<std::shared_ptr<Projectile>> hittedProjectiles;

  for (int i = 0; i < _towers.size(); i++) {
    std::vector<std::shared_ptr<Projectile>> towerHitted =
        _towers[i].get()->updateAndCheckHits(deltaTime);
    hittedProjectiles.insert(hittedProjectiles.end(), towerHitted.begin(),
                             towerHitted.end());
  }
  Reward reward = _waveManager->manageHits(hittedProjectiles);
  _score += reward.points;
  _money += reward.money;
}

bool PlayState::mouseReleased(const SDL_Event *e) {

  if (_buildingTower) {
    if (checkValidNewTowerPosition()) {

      _buildingTower.get()->hideRange();
      _towers.push_back(_buildingTower);
    }
    _buildingTower = NULL;
  }
}

bool PlayState::showMessage(string message) {
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/messageOverlay");
  ov->getChild("message/background")
      ->getChild("message/text")
      ->setCaption(message);
  ov->show();
  _timeSinceLastMessage = 0;
}
bool PlayState::mousePressed(const SDL_Event *e) {

  hideTowerInfo();
  Vector2 pos = OgreUtil::getCursorPosition(e);
  std::cout << "cursor "<<pos <<std::endl;
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/playOverlay");
  OverlayElement *ovElement = ov->findElementAt(pos.x, pos.y);
  
  if (ovElement) {

    hudElementClicked(ovElement, e);
  } else {
    Overlay *ov = mg->getByName("Tower3d/InfoTower");
    OverlayElement *ovElement = ov->findElementAt(pos.x, pos.y);

    if (ovElement && ovElement->getName() == "infoTower/upgradeButton") {
      float price = _towerFactory->getTowerPrice(_selectedTower->getType(),
                                                _selectedTower->getLevel());
      if (price <= _money) {
        _money -= price;

        _towers.erase(
            std::remove(_towers.begin(), _towers.end(), _selectedTower));

        std::shared_ptr<Tower> tower = _towerFactory.get()->createTower(
            _selectedTower->getType(), _numTowers,
            _selectedTower->getLevel() + 1);
        tower->activate();
        tower->setPosition(_selectedTower->getPosition(), false);
        _selectedTower = tower;
        _towers.push_back(tower);
        _numTowers++;
      } else {
        showMessage(NOT_ENOUGH_MONEY);
      }

    } else {
      selectTowerClicked(pos);
    }
  }
  return true;
}

void PlayState::hideTowerInfo() {
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/InfoTower");
  ov->hide();
  if (_selectedTower) {
    _selectedTower->hideRange();
  }
}
void PlayState::selectTowerClicked(Vector2 pos) {

  if (!_buildingTower) {

    Ray mouseRay = _camera->getCameraToViewportRay(pos.x, pos.y);
    _rayScnQuery = _sceneMgr->createRayQuery(mouseRay, 0);
    _rayScnQuery->setRay(mouseRay);
    _rayScnQuery->setSortByDistance(true);
    _rayScnQuery->setQueryMask(TOWER_MASK);
    RaySceneQueryResult &result = _rayScnQuery->execute();
    if (result.begin() != result.end()) {
      RaySceneQueryResult::iterator it = result.begin();

      std::vector<std::shared_ptr<Tower>>::iterator itTower = std::find_if(
          _towers.begin(), _towers.end(), [=](std::shared_ptr<Tower> tower) {
            return tower->getEntity() == (*it).movable;
          });
      if (itTower != _towers.end()) {
        _selectedTower = (*itTower);
        _selectedTower->showProperties(
            _towerFactory->towerHasNextLevel(_selectedTower));
        _selectedTower->showRange();
      }
    }
  }
}

/**
 * Manage clicked Hud elements
 * @parama element OverlayElement what is clicked
 */
bool PlayState::hudElementClicked(OverlayElement *element, const SDL_Event *e) {

  if (boost::algorithm::ends_with(element->getName(), TOWER_ENDING)) {
    // Click in tower Icon, get Tower Type by splitting by / character
    int indexToken = element->getName().rfind("/");
    std::string type = (indexToken >= 0)
                           ? element->getName().substr(indexToken + 1)
                           : element->getName();
    float price = _towerFactory->getTowerPrice(type);

    if (price <= _money){
      _money -= price;
    _buildingTower = _towerFactory.get()->createTower(type, _numTowers);
    _buildingTower->activate();
    _numTowers++;
    }
    else {
      showMessage(NOT_ENOUGH_MONEY);
    }
  }
  return true;
}

PlayState::~PlayState() {}

void PlayState::createScene() {
  createLight();
  _sceneMgr->setAmbientLight(ColourValue(0.3, 0.3, 0.3));

  _sceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE);
  _sceneMgr->setShadowColour(ColourValue(0.5, 0.5, 0.5));
  _sceneMgr->setShadowTextureSize(1024);
  _sceneMgr->setShadowTextureCount(1);

  _sceneMgr->setShadowFarDistance(100);

  _map = MapManager::getSingletonPtr()->load(
      "map1.mapdef",
      std::string(ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME));
}

void PlayState::createLight() {

  Light *light = _sceneMgr->createLight("Light1");
  light->setPosition(3, 20, 0);
  light->setType(Light::LT_SPOTLIGHT);
  light->setDirection(Vector3(0, -1, 0));
  light->setSpotlightInnerAngle(Degree(60.0f));
  light->setSpotlightOuterAngle(Degree(80.0f));

  light->setCastShadows(true);
}

void PlayState::createHUD() {
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/playOverlay");
  ov->show();
}
void PlayState::handleInputEvent(const SDL_Event *event) {

  switch (event->type) {
  case SDL_KEYDOWN:
    keyPressed(event);
    break;
  case SDL_MOUSEMOTION:
    mouseMoved(event);
    break;
  case SDL_MOUSEBUTTONDOWN:
    mousePressed(event);
    break;
  case SDL_MOUSEBUTTONUP:
    mouseReleased(event);
    break;

  case SDL_FINGERDOWN:
    //    mousePressed(event);
    break;
  case SDL_FINGERUP:
    //    mouseReleased(event);
    break;
  }
}
}

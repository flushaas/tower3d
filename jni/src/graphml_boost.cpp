#include "graphml_boost.h"
#include "towerCommon.h"


#
/**
 * BFS visitor
 */
class VisitorBFS : public boost::default_bfs_visitor {
 public:
  // This is invoked when a vertex is encountered for the first time.
  void discover_vertex(graphml_boost::vertex_descriptor_t v,
                       const graphml_boost::graph_t &g) const {
    return;
  }

  // This is invoked on every vertex of the graph before the start of the graph
  // search.
  void initialize_vertex(graphml_boost::vertex_descriptor_t v,
                         const graphml_boost::graph_t &g) const {}

  // This is invoked on a vertex as it is popped from the queue. This happens
  // immediately before examine_edge() is invoked on each of the out-edges of
  // vertex u.
  void examine_vertex(graphml_boost::vertex_descriptor_t v,
                      const graphml_boost::graph_t &g) const {}

  // This is invoked on every out-edge of each vertex after it is discovered.
  void examine_edge(graphml_boost::edge_descriptor_t e,
                    const graphml_boost::graph_t &g) const {}

  // This is invoked on each edge as it becomes a member of the edges that form
  // the search tree.
  void tree_edge(graphml_boost::edge_descriptor_t e,
                 const graphml_boost::graph_t &g) const {}

  // This is invoked on back or cross edges for directed graphs and cross edges
  // for undirected graphs.
  void non_tree_edge(graphml_boost::edge_descriptor_t e,
                     const graphml_boost::graph_t &g) const {}

  // This is invoked on the subset of non-tree edges whose target vertex is
  // colored gray at the time of examination. The color gray indicates that the
  // vertex is currently in the queue.
  void gray_target(graphml_boost::edge_descriptor_t e,
                   const graphml_boost::graph_t &g) const {}

  // This is invoked on the subset of non-tree edges whose target vertex is
  // colored black at the time of examination. The color black indicates that
  // the vertex has been removed from the queue.
  void black_target(graphml_boost::edge_descriptor_t e,
                    const graphml_boost::graph_t &g) const {}

  // This invoked on a vertex after all of its out edges have been added to the
  // search tree and all of the adjacent vertices have been discovered
  //(but before the out-edges of the adjacent vertices have been examined).
  void finish_vertex(graphml_boost::vertex_descriptor_t v,
                     const graphml_boost::graph_t &g) const {}
};

graphml_boost::~graphml_boost() {
  if (_ifstream.is_open())  // En el destructor nos aseguramos de cerrar el
                            // fichero por si acaso.
    _ifstream.close();
}

void graphml_boost::loadGraph(istream *inputStream) {
  //        _ifstream.open(fileName, std::ifstream::in); // Abrimos el fichero
  //        para pasárselo a read_graphml

  // Dynamic Properties
  _dp.property("x", boost::get(&tile_props::x, _graph));
  _dp.property("y", boost::get(&tile_props::y, _graph));
  _dp.property("z", boost::get(&tile_props::z, _graph));
  _dp.property("id", boost::get(&tile_props::id, _graph));
  _dp.property("type", boost::get(&tile_props::type, _graph));
  _dp.property("idarista", boost::get(&way_props::id, _graph));
  _dp.property("source", boost::get(&way_props::source, _graph));
  _dp.property("target", boost::get(&way_props::target, _graph));
  _dp.property("weight", boost::get(&way_props::weight, _graph));

  boost::read_graphml(*inputStream, _graph, _dp);  // Procedemos a la lectura
                                                   // del archivo y lo cargamos
                                                   // en nuestro grafo
}

template <class ParentDecorator>
struct print_parent {
  print_parent(const ParentDecorator &p_) : p(p_) {}
  template <class Vertex>
  void operator()(const Vertex &v) const {
    std::cout << "parent[" << v << "] = " << p[v] << std::endl;
  }
  ParentDecorator p;
};

/**
 * Return all the vertices of a type
 * @param type Type of the vertice to filter by
 *
 */
ruta_t graphml_boost::getVertices(std::string type) {
  ruta_t vertices;
  for (int i = 0; i < boost::num_vertices(_graph); i++) {
    if (std::string(ALL_TYPES) == type || _graph[i].type == type) {
      vertices.push_back(_graph[i]);
    }
  }

  return vertices;
}

/**
 * Get Graph Node corresponding to this position. Y axis distances does not matter.
 * @position current Position
 */
tile_props graphml_boost::getCurrentNode(Ogre::Vector3 position){

   for (int i = 0; i < boost::num_vertices(_graph); i++) {
     Ogre::Vector3 tilePosition = Ogre::Vector3(_graph[i].x,0,_graph[i].z);
     position.y=0;
     if(tilePosition.distance(position) < MAX_ERROR_POSITION){

       return _graph[i];
     }
  }


}

void graphml_boost::calculateRouteBFS(int idSource) {
  boost::graph_traits<graph_t>::vertices_size_type
      d[boost::num_vertices(_graph)];
  std::fill_n(d, boost::num_vertices(_graph), 0);
  _p.clear();
  _p = std::vector<vertex_descriptor_t>(boost::num_vertices(_graph));
  _p[idSource] = idSource;
  boost::breadth_first_search(
      _graph, idSource,  // s,
      boost::visitor(boost::make_bfs_visitor(std::make_pair(
          boost::record_distances(d, boost::on_tree_edge()),
          boost::record_predecessors(&_p[0], boost::on_tree_edge())))));
}

ruta_t graphml_boost::getRuta(size_t idSource, size_t idDestiny)
{
    ruta_t vec;

    if (idDestiny < boost::num_vertices(_graph) && idDestiny >= 0) // Aqu� podr�amos hacer algo m�s sofisticado como lanzar excepciones y esas cosas :D
    {
        size_t i = idDestiny;
        vec.push_back(_graph[idDestiny]);
        while (i != idSource && i >= 0)
        {
            vec.push_back(_graph[_p[i]]);
            i = static_cast<size_t>(_p[i]);
        }
    }

    return vec;

}

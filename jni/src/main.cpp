#include "OgrePlatform.h"
#include "towerCommon.h"
#ifndef RUNTESTS
#include "GameManager.h"
#include "MenuState.h"
#endif
#if RUNTESTS == 1
#include "gtest/gtest.h"
#endif

/**
 * Main Function
 */



               
extern "C" int Tower3d_main(int argc, char *argv[]){

#ifndef RUNTESTS
  int i = 1;
  GameManager *gameMgr = new GameManager();
  
  Tower3d::MenuState *menuState = new Tower3d::MenuState();
  gameMgr->start(menuState);

  delete gameMgr;
#endif
#if RUNTESTS == 1
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
#endif
}



extern "C" int main(int argc, char *argv[]){
  Tower3d_main(argc,argv);

  
}



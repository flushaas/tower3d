#include "OgrePlatform.h"
#include "SoundManager.h"
#include "Sound.h"
#include "towerCommon.h"
#include "OgreLogManager.h"
#include "OgreFileSystemLayer.h"

template <>
Tower3d::SoundManager *Ogre::Singleton<Tower3d::SoundManager>::msSingleton = 0;
namespace Tower3d {

SoundManager *SoundManager::getSingletonPtr() { return msSingleton; }

SoundManager &SoundManager::getSingleton() {
  assert(msSingleton);
  return (*msSingleton);
}

SoundManager::SoundManager() {
    /* Open the audio device, forcing the desired format */
  if (  Mix_OpenAudio(22050, AUDIO_S16,2, 4096) < 0) {
    fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());

  }


}

SoundManager::~SoundManager() {

  void Mix_CloseAudio(void);

}

  Mix_Chunk* SoundManager::loadSound(std::string sound){
    #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
      Ogre::FileSystemLayer* mFSLayer = OGRE_NEW_T(
      Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)(OGRE_VERSION_NAME);
      std::string soundPath = "sounds/"+mFSLayer->getConfigFilePath(sound);
    #endif
    #if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
          std::string soundPath= "../assets/sounds/"+sound;
    #endif

    Mix_Chunk * chunk =  Mix_LoadWAV(soundPath.c_str());
    if(!chunk){
    std::stringstream str;
    str <<"----FALLO Cargando Sonido " << soundPath <<" :"<< Mix_GetError() << std::endl;
    Ogre::LogManager::getSingleton().logMessage(str.str());
    
    }
    else{
      std::stringstream str;
      str <<"----------Sonido Cargado" << soundPath  << std::endl;
      Ogre::LogManager::getSingleton().logMessage(str.str());
    }
    return chunk;

  }

  void SoundManager::playFX(Sound* sound){
    if(Mix_PlayChannel(-1, sound->getChunk(), 0)==-1){
      std::stringstream str;
       str << " Error Cargando Sonido "<< Mix_GetError() << std::endl;
       Ogre::LogManager::getSingleton().logMessage(str.str() );
    }
  }
  void SoundManager::playMusic(Sound* music){
    Mix_PlayChannel(1, music->getChunk(), 0);

  }
  void SoundManager::stopMusic(){
    Mix_FadeOutChannel(1,2000);
  }
     


}

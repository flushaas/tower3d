#include <Ogre.h>
#include "towerCommon.h"
#include "GameManager.h"
#include "OgreOverlaySystem.h"
#include "OgreOverlayManager.h"
#include "OgreParticleSystemManager.h"
#include "GameState.h"
#include "SDL.h"
#include "SDL_syswm.h"
#include "SDL_version.h"
#include "OgreCommon.h"
#include "OgreFileSystemLayer.h"
#include "SoundManager.h"
#include "OgreLogManager.h"
#include "OgreProfiler.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID


#include <jni.h>
#include <android/api-level.h>
#include <android/native_window_jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include "OgreOctreePlugin.h"
#include "OgreDataStream.h"
#include "Android/OgreAPKFileSystemArchive.h"
#include "Android/OgreAPKZipArchive.h"
#include <OgreShaderGenerator.h>
#include "OgreArchiveManager.h"
//#ifdef OGRE_BUILD_PLUGIN_PFX
#include "OgreParticleFXPlugin.h"
//#endif
#ifdef OGRE_BUILD_COMPONENT_OVERLAY
#include "OgreOverlaySystem.h"
#endif

#include "OgreConfigFile.h"

#ifdef OGRE_BUILD_RENDERSYSTEM_GLES2
#include "OgreGLES2Plugin.h"
#define GLESRS GLES2Plugin
#else
#include "OgreGLESPlugin.h"
#define GLESRS GLESPlugin
#endif

/**
 * @brief Native Window created by Android
 */
extern  ANativeWindow* native_window ;

/**
 * @brief Method to open a file inside Assets
 * @param fileName file name to open
 */
Ogre::DataStreamPtr openAPKFile(const Ogre::String& fileName) {
  extern AAssetManager* assetMgr;
  Ogre::DataStreamPtr stream;
  AAsset* asset =
      AAssetManager_open(assetMgr, fileName.c_str(), AASSET_MODE_BUFFER);
  if (asset) {
    off_t length = AAsset_getLength(asset);
    void* membuf = OGRE_MALLOC(length, Ogre::MEMCATEGORY_GENERAL);
    memcpy(membuf, AAsset_getBuffer(asset), length);
    AAsset_close(asset);

    stream = Ogre::DataStreamPtr(
        new Ogre::MemoryDataStream(membuf, length, true, true));
  }
  return stream;

}

/**
 * @brief Shader Generator for Android
 */
class ShaderGeneratorTechniqueResolverListener
    : public Ogre::MaterialManager::Listener {
 public:
  ShaderGeneratorTechniqueResolverListener(
      Ogre::RTShader::ShaderGenerator* pShaderGenerator) {
    mShaderGenerator = pShaderGenerator;
  }

  /** This is the hook point where shader based technique will be created.
  It will be called whenever the material manager won't find appropriate
  technique
  that satisfy the target scheme name. If the scheme name is out target RT
  Shader System
  scheme name we will try to create shader generated technique for it.
  */
  virtual Ogre::Technique* handleSchemeNotFound(
      unsigned short schemeIndex, const Ogre::String& schemeName,
      Ogre::Material* originalMaterial, unsigned short lodIndex,
      const Ogre::Renderable* rend) {
    Ogre::Technique* generatedTech = NULL;

    // Case this is the default shader generator scheme.
    if (schemeName == Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME) {
      bool techniqueCreated;

      // Create shader generated technique for this material.
      techniqueCreated = mShaderGenerator->createShaderBasedTechnique(
          originalMaterial->getName(),
          Ogre::MaterialManager::DEFAULT_SCHEME_NAME, schemeName);

      // Case technique registration succeeded.
      if (techniqueCreated) {
        // Force creating the shaders for the generated technique.
        mShaderGenerator->validateMaterial(schemeName,
                                           originalMaterial->getName());

        // Grab the generated technique.
        Ogre::Material::TechniqueIterator itTech =
            originalMaterial->getTechniqueIterator();

        while (itTech.hasMoreElements()) {
          Ogre::Technique* curTech = itTech.getNext();

          if (curTech->getSchemeName() == schemeName) {
            generatedTech = curTech;
            break;
          }
        }
      }
    }

    return generatedTech;
  }

 protected:
  Ogre::RTShader::ShaderGenerator*
      mShaderGenerator;  // The shader generator instance.
};

#endif

using namespace std;
using namespace Ogre;









template <>
GameManager *Ogre::Singleton<GameManager>::msSingleton = 0;

GameManager::GameManager() { _root = 0; }

GameManager::~GameManager() {
  while (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  if (_mapManager) delete _mapManager;
  #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
  std::for_each(_plugins.begin(),_plugins.end(),[this](Plugin* plugin){this->_root->uninstallPlugin(plugin);});
  #endif
  if (_root) delete _root;
}



void GameManager::handleInputEvent(const SDL_Event *event){
 _states.top()->handleInputEvent(event);

}

void GameManager::showLoadingImage() {

  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

      #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
      Ogre::FileSystemLayer* mFSLayer = OGRE_NEW_T(
      Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)(OGRE_VERSION_NAME);
       SDL_Surface* surface = SDL_LoadBMP(mFSLayer->getConfigFilePath("loading.bmp").c_str());
				     				      
  #endif
  // Load image as SDL_Surface
  #if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
      SDL_Surface* surface = SDL_LoadBMP("loading.bmp");				      

  #endif

      // SDL_Surface is just the raw pixels
      // Convert it to a hardware-optimzed texture so we can render it
      SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);

      // Don't need the orignal texture, release the memory
      SDL_FreeSurface(surface);
      SDL_RenderCopy(renderer, texture, NULL, NULL);

      SDL_RenderPresent(renderer);
      SDL_GL_SwapWindow(window);
      SDL_DestroyTexture(texture);
      SDL_DestroyRenderer(renderer);
}
void GameManager::start(GameState *state) {
   
  // Ogre::NameValuePairList misc;
  // misc["currentGLContext"] = Ogre::String("True");
   SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
   SDL_SysWMinfo info;
  SDL_VERSION(&info.version);
  Mix_AllocateChannels(16);
  window = SDL_CreateWindow("My Game Window",
                          SDL_WINDOWPOS_UNDEFINED,
                          SDL_WINDOWPOS_UNDEFINED,
                          1024, 600,
                          SDL_WINDOW_OPENGL);

  //  renderer = SDL_CreateRenderer(window, -1, 0);

    /*    SDL_Rect rect;
    rect.x =0 ;
    rect.y=0;
    rect.w=100;
    rect.h=100;
    SDL_SetRenderDrawColor(SDL_GetRenderer(window), 255, 0, 0, 255);
    SDL_RenderFillRect(SDL_GetRenderer(window),rect);
    */
    
    
  SDL_GetWindowWMInfo(window,&info);
  showLoadingImage();
  SDL_GLContext glcontext = SDL_GL_CreateContext(window);

    /*
   
   size_t winHandle = reinterpret_cast<size_t>(native_window);
    size_t glHandle = reinterpret_cast<size_t>(glcontext);

    std::stringstream str;


    Ogre::NameValuePairList opt;
    opt["currentGLContext"]     = "True";
    //    str << (int) native_window;
    opt["externalWindowHandle"] =Ogre::StringConverter::toString((unsigned long)winHandle);
    opt["externalGLContext"] =Ogre::StringConverter::toString((unsigned long )glcontext);

  
  */

    /*
 
// Create an OpenGL context associated with the window.
SDL_GLContext glcontext = SDL_GL_CreateContext(screen);
 stringstream str;
 str << (int) SDL_GL_GetCurrentContext();
 misc["externalGLContext"]    = str.str();

 opt["externalWindowHandle"] = Ogre::StringConverter::toString( (int)screen );
 
 if(!glcontext){
   LOGW(SDL_GetError());


 }
 else{
   LOGW("Existe contesto GL");
 }
*/
  // Creación del objeto Ogre::Root.
  #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
    _root = new Ogre::Root("","","");
  #endif
  #if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
    _root = new Root();
  #endif
    //   new Ogre::StaticPluginLoader()->load();


  // Register Custom Resource Managers
  _mapManager = new Tower3d::MapManager();
  //     Ogre::ParticleSystemManager *manager = new Ogre::ParticleSystemManager;
  //  Ogre::OverlayManager *overLayManager = new Ogre::OverlayManager;
  // ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

  loadResources();
  if (!configure()) return;

  _root->initialise(false, "Tower3d");

   SDL_GLContext m_sdl_gl_context = SDL_GL_GetCurrentContext();      
  Ogre::NameValuePairList opt;

  #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
  JNIEnv* env = (JNIEnv*)SDL_AndroidGetJNIEnv();


    jclass class_sdl_activity   = env->FindClass("org/libsdl/app/SDLActivity");
    jmethodID method_get_native_surface = env->GetStaticMethodID(class_sdl_activity, "getNativeSurface", "()Landroid/view/Surface;");
    jobject raw_surface = env->CallStaticObjectMethod(class_sdl_activity, method_get_native_surface);
    ANativeWindow* native_window = ANativeWindow_fromSurface(env, raw_surface);

    if ( !native_window )
        return;
    opt["externalWindowHandle"] = Ogre::StringConverter::toString( (uint)native_window );
#endif
    std::string aux = Ogre::StringConverter::toString( (ulong)m_sdl_gl_context );

     opt["currentGLContext"]     = "true";
     opt["externalGLContext"]    = aux;


   _renderWindow = Ogre::Root::getSingleton().createRenderWindow("OgreWindow", 0, 0, false, &opt);
       _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, MAIN_SCN_MGR);

  Ogre::OverlaySystem *overlaySystem = new Ogre::OverlaySystem();
  _sceneMgr->addRenderQueueListener(overlaySystem);
  

  new Ogre::Profiler();


   #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
              Ogre::RTShader::ShaderGenerator::initialize();

          // The Shader generator instance
          Ogre::RTShader::ShaderGenerator* gen =
              Ogre::RTShader::ShaderGenerator::getSingletonPtr();


    ShaderGeneratorTechniqueResolverListener*  material_mgr_listener_ =
                new ShaderGeneratorTechniqueResolverListener(gen);
            Ogre::MaterialManager::getSingleton().addListener(
                material_mgr_listener_);
 gen->addSceneManager(_sceneMgr);	    
	    #endif



  
    // _renderWindow = _root->createRenderWindow("OgreWindow", 0, 0, false, &opt);
  _renderWindow->setVisible(true);

  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

  new Tower3d::SoundManager();
  _inputMgr = new Tower3d::InputManager;


  _inputMgr->addInputHandler("GameManager",this);


    // El GameManager es un FrameListener.
  _root->addFrameListener(this);

  // Transición al estado inicial.

  //
  //    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

        // Clear the entire screen to our selected color.
  //	    SDL_RenderClear(renderer);

  Ogre::Camera * camera =  _sceneMgr->createCamera("MainCamera");



  camera->setPosition(Vector3(0, 15, -0.1));

  camera->lookAt(_sceneMgr->getRootSceneNode()->getPosition());

  camera->lookAt(0, 0, 0);

  camera->setNearClipDistance(0.1);

  camera->setFarClipDistance(1000);
  Ogre::Viewport *viewport = _root->getRenderTarget("OgreWindow")->addViewport(camera);
      // Configuramos la camara
  double width = viewport->getActualWidth();

  double height = viewport->getActualHeight();
  camera->setAspectRatio(width / height);
   _sceneMgr->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
   changeState(state);

  // Bucle de rendering.
  _root->startRendering();
}

void GameManager::changeState(GameState *state) {

  // Limpieza del estado actual.
  if (!_states.empty()) {

    // exit() sobre el último estado.
    _states.top()->exit();
    // Elimina el último estado.
    _states.pop();
  }

  // Transición al nuevo estado.
  _states.push(state);

  // enter() sobre el nuevo estado.
  _states.top()->enter();

}

void GameManager::pushState(GameState *state) {
  // Pausa del estado actual.
  if (!_states.empty()) _states.top()->pause();

  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void GameManager::popState() {
  // Limpieza del estado actual.
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }

  // Vuelta al estado anterior.
  if (!_states.empty()) _states.top()->resume();
}

void GameManager::loadResources() {
  Ogre::ConfigFile cf;
  #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
      JNIEnv* env = (JNIEnv*)SDL_AndroidGetJNIEnv();
    jclass class_activity       = env->FindClass("android/app/Activity");
    jclass class_resources      = env->FindClass("android/content/res/Resources");
    jmethodID method_get_resources      = env->GetMethodID(class_activity, "getResources", "()Landroid/content/res/Resources;");
    jmethodID method_get_assets         = env->GetMethodID(class_resources, "getAssets", "()Landroid/content/res/AssetManager;");
    jobject raw_activity = (jobject)SDL_AndroidGetActivity();
    jobject raw_resources = env->CallObjectMethod(raw_activity, method_get_resources);
    jobject raw_asset_manager = env->CallObjectMethod(raw_resources, method_get_assets);
    AAssetManager* asset_manager = AAssetManager_fromJava(env, raw_asset_manager);

    if (asset_manager)
    {
        Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKFileSystemArchiveFactory(asset_manager) );
        Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKZipArchiveFactory(asset_manager) );
    }
    Ogre::FileSystemLayer* mFSLayer = OGRE_NEW_T(
      Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)(OGRE_VERSION_NAME);

  cf.load(openAPKFile(mFSLayer->getConfigFilePath("resources_android.cfg")));
  #endif
  #if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
  cf.load("../assets/resources.cfg");
  #endif

  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;
      datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
          datastr, typestr, sectionstr);
    }
  }
}

bool GameManager::configure() {
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID

    if (!_root->restoreConfig()) {
  
    if (!_root->showConfigDialog()) {
      return false;
    }
  }
    #endif


  #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

  #ifdef OGRE_BUILD_RENDERSYSTEM_GLES2
  Ogre::Plugin *gGLESPlugin = OGRE_NEW GLESRS();
  _root->installPlugin(gGLESPlugin);
  _plugins.push_back(gGLESPlugin);
  #endif

#ifdef OGRE_BUILD_PLUGIN_OCTREE
  Ogre::Plugin * gOctreePlugin = OGRE_NEW Ogre::OctreePlugin();
  _root->installPlugin(gOctreePlugin);
  _plugins.push_back(gOctreePlugin);
#endif

  #ifdef OGRE_BUILD_PLUGIN_PFX
  Ogre::Plugin* gParticleFXPlugin = OGRE_NEW ParticleFXPlugin();
  _root->installPlugin(gParticleFXPlugin);
  _plugins.push_back(gParticleFXPlugin);
  #endif

#endif


  
  _root->setRenderSystem(_root->getAvailableRenderers().at(0));
  


  return true;
}

GameManager *GameManager::getSingletonPtr() { return msSingleton; }

GameManager &GameManager::getSingleton() {
  assert(msSingleton);
  return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.
bool GameManager::frameStarted(const Ogre::FrameEvent &evt) {



  // Select the color for drawing. It is set to red here.
  //SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

        // Clear the entire screen to our selected color.
	//    SDL_RenderClear(renderer);

  //    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

        // Clear the entire screen to our selected color.
    //	    SDL_RenderClear(renderer);


	    //   SDL_RenderPresent(renderer);
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
   SDL_GL_SwapWindow(window);
#endif
  _inputMgr->capture();

  return _states.top()->frameStarted(evt);
}

bool GameManager::frameEnded(const Ogre::FrameEvent &evt) {
  return _states.top()->frameEnded(evt);
}


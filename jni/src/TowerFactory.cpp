#include "TowerFactory.h"
#include "Tower.h"
#include <sstream>
#include <string>
#include "OgreConfigFile.h"
#include "OgreStringConverter.h"
#include "OgreFileSystemLayer.h"

using namespace std;
using namespace Ogre;
Ogre::DataStreamPtr openAPKFile(const Ogre::String &fileName);
namespace Tower3d {

/**
 * Default constructor. It parses towers.cfg file for towers especification
 */

TowerFactory::TowerFactory() {
  Ogre::ConfigFile cf;

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
  Ogre::FileSystemLayer *mFSLayer = OGRE_NEW_T(
      Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)(OGRE_VERSION_NAME);

  cf.load(openAPKFile(mFSLayer->getConfigFilePath("towers.cfg")));
#endif
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
  cf.load("../assets/towers.cfg");
#endif

  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String towerName;
  while (sI.hasMoreElements()) {
    towerName = sI.peekNextKey();

    std::map<string, string> towerProperties;
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      towerProperties.insert(pair<string, string>(i->first, i->second));
    }
    _towerDefs.insert(
        pair<string, std::map<string, string>>(towerName, towerProperties));
  }
}

/**
 * Create a Tower of a especified type with the especified id
 * @param towerType Type of tower to be created
 * @param id Identifier ot the tower
 */
std::unique_ptr<Tower> TowerFactory::createTower(string towerType, int id,
                                                 int level) {
  if (level > 0) {
    stringstream str;
    str << towerType << level;
    towerType = str.str();
  }
  std::map<string, string> towerProperties = _towerDefs.at(towerType);
  float range = atof(towerProperties.at("range").c_str());
  float timeReload = atof(towerProperties.at("timeReload").c_str());
  float damage = atof(towerProperties.at("damage").c_str());
  float splashArea = atof(towerProperties.at("splashArea").c_str());
  string meshName = towerProperties.at("meshName");
  string projectileMeshName = towerProperties.at("projectileMeshName");
  Vector3 shootOrigin =
      StringConverter::parseVector3(towerProperties.at("shootPosition"));
  Vector3 initialOffset =
      StringConverter::parseVector3(towerProperties.at("initialOffset"));
  Vector3 initialScale =
      StringConverter::parseVector3(towerProperties.at("initialScale"));

  float projectileSpeed = atof(towerProperties.at("projectileSpeed").c_str());
  string shootSound = towerProperties.at("shootSound");
  float rotationSpeed = atof(towerProperties.at("rotationSpeed").c_str());
  float price = atof(towerProperties.at("price").c_str());
  stringstream str;
  str << "Tower" << id;
  return std::unique_ptr<Tower>(new Tower(
      str.str(), range, timeReload, damage, splashArea, meshName,
      projectileMeshName, shootOrigin, projectileSpeed, shootSound,
      rotationSpeed, initialOffset, initialScale, price, towerType, level));
}
}

#include "InputManager.h"
#include "towerCommon.h"

namespace Tower3d {
template <> InputManager *Ogre::Singleton<InputManager>::msSingleton = 0;

InputManager::InputManager(){}

InputManager::~InputManager() { _inputHandlers.clear(); }


void InputManager::capture() {
  SDL_Event event;

  while (SDL_PollEvent(&event)) {

    std::for_each(_inputHandlers.begin(), _inputHandlers.end(),
                  [event](std::pair<std::string, InputHandler*> pairInput) {
                    pairInput.second->handleInputEvent(&event);
                  });
  }
}

void InputManager::addInputHandler(const std::string &instanceName,
                                   InputHandler *inputHandler) {
  if (!_inputHandlers.count(instanceName)) {
    _inputHandlers.insert(std::pair<std::string, InputHandler*>(instanceName, inputHandler));
  }
}

void InputManager::removeInputHandler(const std::string &instanceName) {
  if (_inputHandlers.count(instanceName)) {
    _inputHandlers.erase(instanceName);
  }
}

void InputManager::removeAllInputHandlers() { _inputHandlers.clear(); }

InputManager *InputManager::getSingletonPtr() { return msSingleton; }

InputManager &InputManager::getSingleton() {
  assert(msSingleton);
  return *msSingleton;
}
}

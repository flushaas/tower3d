#include "EnemyFactory.h"
#include "Enemy.h"
#include <sstream>
#include <string>
#include <exception>
#include "OgreConfigFile.h"
#include "OgreFileSystemLayer.h"

using namespace std;
  Ogre::DataStreamPtr openAPKFile(const Ogre::String& fileName);
namespace Tower3d {


  /**
   * Throws when enemy type is no found
   * 
   */
struct EnemyNotFoundException : std::exception {
  /**
   * enemy type that is not defined
   */
  string _enemyType;

  /**
   * Constructor
   * @param EnemyType enemy Type not found
   */
  EnemyNotFoundException(string enemyType):_enemyType(enemyType){}
  const char* what() const noexcept {return ("Enemy Type not configured :"+_enemyType).c_str();}
};
/**
 * Default constructor. It parses enemys.cfg file for enemys especification
 */

  EnemyFactory::EnemyFactory(MapPtr map):_map(map) {
  Ogre::ConfigFile cf;
      #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
      Ogre::FileSystemLayer* mFSLayer = OGRE_NEW_T(
      Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)(OGRE_VERSION_NAME);

  cf.load(openAPKFile(mFSLayer->getConfigFilePath("enemys.cfg")));
#endif
   #if  OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
  cf.load("../assets/enemys.cfg");
  #endif



  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String enemyName;
  while (sI.hasMoreElements()) {
    enemyName = sI.peekNextKey();

    std::map<string, string> enemyProperties;
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      enemyProperties.insert(pair<string, string>(i->first, i->second));
    }
    _enemyDefs.insert(
        pair<string, std::map<string, string>>(enemyName, enemyProperties));
  }
  this->_route = _map->getRoute(_map->getStartPosition());

}

/**
 * Create a Enemy of a especified type with the especified id
 * @param enemyType Type of enemy to be created
 * @param id Identifier ot the enemy
 * @param time Time of the enemy
 */
  std::shared_ptr<Enemy> EnemyFactory::createEnemy(string enemyType, string  id, long time) {
  if(!_enemyDefs.count(enemyType)){
    throw new EnemyNotFoundException(enemyType);

  }
  std::map<string, string> enemyProperties = _enemyDefs.at(enemyType);

  float speed = atof(enemyProperties.at("speed").c_str());
  float life = atof(enemyProperties.at("life").c_str());
  string meshName = enemyProperties.at("meshName");
  float rotateY = atof(enemyProperties.at("rotateY").c_str());
  float points = atof(enemyProperties.at("points").c_str());
  float money = atof(enemyProperties.at("money").c_str());
  

  return std::shared_ptr<Enemy>(
				new Enemy(id, speed, life, meshName, time,_route,rotateY,enemyType,points,money));
}
}

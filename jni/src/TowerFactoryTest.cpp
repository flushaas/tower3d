#include "OgrePlatform.h"
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
#include "gtest/gtest.h"
#include "TowerFactory.h"

TEST(TOWERFACTORYTESTCASE, TOWERFACTORYLOADTEST) {
  try {
    Tower3d::TowerFactory* towerFactory = new Tower3d::TowerFactory();
    std::unique_ptr<Tower3d::Tower> tower =
        towerFactory->createTower("CanonTower", 1);

    EXPECT_EQ(10, tower.get()->getRange()) << "El Rango no coincide"
                                           << std::endl;
    EXPECT_EQ(20, tower.get()->getDamage()) << "El da�o no coincide "
                                            << std::endl;
    EXPECT_EQ(2, tower.get()->getTimeReload())
        << "El tiempo de recarga no coincide " << std::endl;
    EXPECT_EQ(3, tower.get()->getSplashArea())
        << "El area de splash no coincide no coincide " << std::endl;
    EXPECT_EQ("torreCanon.mesh", tower.get()->getMeshName())
        << "El nombre de la malla no coincide " << std::endl;

  } catch (std::exception& se) {
    FAIL() << se.what() << std::endl;
  }
}
#endif

#include "Wave.h"
#include "EnemyFactory.h"
#include <sstream>
#include <ctime>

#define ENEMY_TYPE "enemyType"
#define ENEMY_INTERVAL "enemyInterval"
#define ENEMY_NUMBER "enemyNumber"
#define ENEMY_DELAY "enemyDelay"
using namespace std;
namespace Tower3d{
  
  Wave::Wave(t_waveProperties waveProperties,shared_ptr<EnemyFactory> enemyFactory,long startWaveTime):_enemyFactory(enemyFactory),_startWaveTime(startWaveTime) {

    std::cout << "STart time "  << _startWaveTime << std::endl;
  int subWavesIdx = 1;
  bool lastSubWaveFound = false;
  long currentTime = startWaveTime;
  _idWave = waveProperties.at("idWave");
  while (!lastSubWaveFound) {

    stringstream str;
    str << ENEMY_TYPE << subWavesIdx;
    string enemyTypeKey = str.str();
    if (waveProperties.count(enemyTypeKey)) {
      str.str("");
      str << ENEMY_INTERVAL << subWavesIdx;
      string enemyIntervalKey = str.str();
      str.str("");
      str << ENEMY_NUMBER << subWavesIdx;
      string enemyNumberKey = str.str();
      str.str("");
      str << ENEMY_DELAY << subWavesIdx;
      string delayKey = str.str();
      if(waveProperties.count(delayKey) > 0){
	float  delay = atof(waveProperties.at(delayKey).c_str());
	std::cout << "delay" << delay << " "<<currentTime << std::endl;
	currentTime+=(long)delay;
 	std::cout << "delay" << delay << " "<<currentTime << std::endl;

      }
      currentTime = addEnemies(waveProperties.at(enemyTypeKey),
			       atoi(waveProperties.at(enemyNumberKey).c_str()),
			       atof(waveProperties.at(enemyIntervalKey).c_str()), subWavesIdx,currentTime);
      subWavesIdx++;

    } else {
      lastSubWaveFound = true;
    }
  }
  _aproxEndTime = currentTime + (long)TIME_OFFSET_END;
}

  /**
   * Create enemies of the SubWave.
   * @param enemyType Type of the enemy
   * @param numberOfEnemies Nuber of enemies of this type
   * @param interval intervla between enemies of this subways
   * @param subWaveIdx index of the subway
   */
long Wave::addEnemies(string enemyType, int numberOfEnemies, float interval,
		      int subWaveIdx, long  nextEnemyTime) {
  //  std::cout << "Tamanio enemigos"<< numberOfEnemies<< std::endl;

  for (int i = 0; i < numberOfEnemies; i++) {
    stringstream str;
    str << "Enemy" << i << "Wave" << _idWave << "Sub" <<subWaveIdx;
    _enemies.push_back(_enemyFactory->createEnemy(enemyType, str.str(),nextEnemyTime));
    nextEnemyTime+=(long)interval;
    std::cout <<nextEnemyTime << std::endl;
    

  }
  return nextEnemyTime;
}
int Wave::update(double deltaTime) {

  int numberOfLivesTaken = 0;
  //  std::cout << "Tamanio enemigos"<< _enemies.size()<< std::endl;
  for(int i = 0; i< _enemies.size(); i++){
    //    std::cout << "Actualizando Enemigo Wave"<<  std::endl;
    if(_enemies[i].get()->update(deltaTime)){
      _enemies.erase(_enemies.begin()+i);
      numberOfLivesTaken++;

    }

  }
  return numberOfLivesTaken;

}
  
  Reward Wave::manageHits(std::vector<std::shared_ptr<Projectile>> projectiles){
    Reward reward;
    reward.money=0;
    reward.points=0;
    Reward* rewptr = &reward;
    


    std::for_each(projectiles.begin(), projectiles.end(),[=](std::shared_ptr<Projectile> projectile) mutable {

	_enemies.erase(remove_if(this->_enemies.begin(),this->_enemies.end(),[projectile,rewptr](std::shared_ptr<Enemy> enemy)mutable{

	      if(enemy.get()->getActive()){

	  if(enemy.get()->getPosition().distance(projectile.get()->getDestiny()) <= projectile.get()->getSplashArea()){
	    //if hit return true, enemy is dead
	    if( enemy.get()->hit(projectile.get()->getDamage())){
	      rewptr->money+=enemy.get()->getMoney();
	      rewptr->points= enemy.get()->getPoints();

	      return true;
	    }
	    else{
	      return false;
	    }

	  }
	  else {
	    return false;
	  }
	      }

	    }),_enemies.end());
	  

    });

    return reward;



}

  
}

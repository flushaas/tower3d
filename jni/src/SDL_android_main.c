/*
extern C_LINKAGE int main(int argc, char *argv[]){
  Tower3d_main(argc,argv);


}

    SDL_android_main.c, placed in the public domain by Sam Lantinga  3/13/14
*/



#ifdef __ANDROID__



/* Include the SDL main definition header */
#include "SDL_main.h"

#include <android/native_window_jni.h>


/*******************************************************************************
                 Functions called by JNI
*******************************************************************************/
#include <jni.h>
extern  ANativeWindow* native_window = 0;

/* Called before SDL_main() to initialize JNI bindings in SDL library */
extern void SDL_Android_Init(JNIEnv* env, jclass cls); 

/* Start up the SDL app */
JNIEXPORT int JNICALL Java_org_libsdl_app_SDLActivity_nativeInit(JNIEnv* env, jclass cls, jobject array)
{
  jclass class_sdl_activity   = (*env)->FindClass(env,"org/libsdl/app/SDLActivity");
  jmethodID method_get_native_surface = (*env)->GetStaticMethodID(env,class_sdl_activity, "getNativeSurface", "()Landroid/view/Surface;");
  jobject raw_surface = (*env)->CallStaticObjectMethod(env,class_sdl_activity, method_get_native_surface);
    native_window = ANativeWindow_fromSurface(env, raw_surface);

    if ( !native_window )
        return;
  
    int i;
    int argc;
    int status;
    int len;
    char** argv;

    /* This interface could expand with ABI negotiation, callbacks, etc. */
    SDL_Android_Init(env, cls);

    SDL_SetMainReady();





    /* Run the application. */

    status = SDL_main(argc, argv);

    /* Release the arguments. */

    for (i = 0; i < argc; ++i) {
        SDL_free(argv[i]);
    }
    SDL_stack_free(argv);
    /* Do not issue an exit or the whole application will terminate instead of just the SDL thread */
    /* exit(status); */

    return status;
}

/*extern C_LINKAGE int main(int argc, char *argv[]){
  Tower3d_main(argc,argv);


  }*/


#endif /* __ANDROID__ */



/* vi: set ts=4 sw=4 expandtab: */

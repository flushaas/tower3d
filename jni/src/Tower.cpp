#include "Tower.h"
#include "OgreVector3.h"
#include "Projectile.h"
#include "OgreBillboardSet.h"
#include "OgreManualObject.h"
#include "OgreMovableObject.h"
#include "Enemy.h"
#include <memory>
#include <string>
#include <iterator>
#include "OgreProfiler.h"
#include "OgreOverlayManager.h"
#include "OgreOverlayContainer.h"

using namespace Ogre;



namespace Tower3d {

void Tower::activate() {

  SceneManager *scnMgr = Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);
  SceneNode *mainNode = scnMgr->createSceneNode(_id + "main");
  bset = scnMgr->createBillboardSet();
  bset->setMaterialName("rangeMaterial");
  Billboard *bBoard = bset->createBillboard(Vector3(0, 0, 0));
  bBoard->setDimensions(_range * 3, _range * 3);
  mainNode->attachObject(bset);

  Entity *ent = scnMgr->createEntity(_id, _meshName);
  ent->setCastShadows(true);
  this->_node = scnMgr->createSceneNode(_id);
  this->_node->attachObject(ent);
  mainNode->addChild(this->_node);

  SceneNode *shootNode = scnMgr->createSceneNode(_id + "Shoot");
  this->_node->addChild(shootNode);
  this->_node->scale(_initialScale);

  scnMgr->getRootSceneNode()->addChild(mainNode);
  shootNode->setPosition(_shootOrigin);
  showRange();
}

void Tower::shoot() {

  Vector3 enemyPosition = _targetedEnemy.get()->getPosition();

  SoundManager::getSingletonPtr()->playFX(_shootSound);
  stringstream str;
  SceneManager *scnMgr = Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);
  SceneNode *shootNode = scnMgr->getSceneNode(_id + "Shoot");
  str << _id << "Projectile" << _projectileCount;
  std::string idProjectile = str.str();
  _activeProjectiles.push_back(std::shared_ptr<Projectile>(

      new Projectile(shootNode->_getDerivedPosition(), enemyPosition,
                     _projectileSpeed, _projectileMeshName, idProjectile,
                     _damage, _splashArea)));

  _projectileCount++;
}

void Tower::checkEnemyOnRange(shared_ptr<Enemy> enemy, float deltaTime) {

  bool aux=false;
  if (!_targetedEnemy || !_targetedEnemy.get()->getActive() ||
      _targetedEnemy.get()->getPosition().distance(_node->getParentSceneNode()->getPosition()) >
          _range) {
    if(_targetedEnemy){
      std::cout << "Enmigo perdido " << _targetedEnemy->getId() << std::endl;
      _targetedEnemy.reset();
    }


    if (_node->getParentSceneNode()->getPosition().distance(enemy.get()->getPosition()) < _range) {
      
      _targetedEnemy = enemy;
      aux=true;
      std::cout << "Enmigo localizado " << _targetedEnemy->getId() << std::endl;
    }
  }
  if (_targetedEnemy) {
    Vector3 xaxis = -_node->getLocalAxes().GetColumn(0);
    xaxis.y = 0;
    xaxis.normalise();

    
    Vector3 position = _targetedEnemy.get()->getPosition()- _node->getParentSceneNode()->getPosition();
   
     position.y = 0;
    position.normalise();


    float degrees = Degree(position.angleBetween(xaxis)).valueDegrees();

    //std::cout << "EnemyPos " <<  << "-"<< xaxis << std::endl;
    //std::cout << "Grados " << degrees <<" entre "  << position << "-"<< xaxis << std::endl;

    
    float rotDegrees = _rotationSpeed * deltaTime;

    //    std::cout << "rotGrados " << rotDegrees << std::endl;
    if (std::abs(degrees) < std::abs(rotDegrees)) {
      rotDegrees = degrees;
    }
    if (xaxis.crossProduct(position).y < 0) {
      rotDegrees = -rotDegrees;
    }

    if (degrees < TOLERANCE_DEGREES && _timeSinceLastShoot > _timeReload) {
      _timeSinceLastShoot = 0;

      shoot();
    }
    if(aux)
      std::cout << "rotGrados " << rotDegrees << std::endl;
    _node->yaw(Radian(Degree(rotDegrees)));
  }
}
void Tower::setValidPosition(bool valid) {
  if (valid != _validPosition) {
    _validPosition = valid;
    if (_validPosition) {
      bset->setMaterialName("rangeMaterial");
    } else {
      bset->setMaterialName("rangeMaterialRed");
    }
  }
}

std::vector<std::shared_ptr<Projectile>>
Tower::updateAndCheckHits(float deltaTime) {
  _timeSinceLastShoot += deltaTime;

  if (_targetedEnemy && !_targetedEnemy.get()->getActive()) {
    _targetedEnemy.reset();
  }

  std::vector<std::shared_ptr<Projectile>> hittedProjectiles;
  std::vector<std::shared_ptr<Projectile>>::iterator it =
      _activeProjectiles.begin();
  while (it != _activeProjectiles.end()) {
    if ((*it).get()->updateAndCheck(deltaTime)) {
      hittedProjectiles.push_back(*it);
      it = _activeProjectiles.erase(it);

    } else {
      it++;
    }
  }

  return hittedProjectiles;
}

void Tower::showProperties(bool hasNextLevel) {

  float x, y;
  SceneManager *scnMgr = Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);
  Camera *_camera = scnMgr->getCamera("MainCamera");
  OgreUtil::projectPos(_camera,
                       this->_node->getParentSceneNode()->getPosition(), x, y);
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/InfoTower");

  //Make sure that the Tower Info panel is not out of screen or behind button
  if (x > 0.7)
    x -= 0.3;
  if (y > 0.5)
    y -= 0.3;
  ov->getChild("infoTower/infoContainer")->setPosition(x, y);

  stringstream str;
  str << _level;

  ov->getChild("infoTower/infoContainer")
      ->getChild("infoTower/levelValue")
      ->setCaption(str.str());
  str.str("");
  str << _range;
  ov->getChild("infoTower/infoContainer")
      ->getChild("infoTower/rangeValue")
      ->setCaption(str.str());
  str.str("");
  str << _splashArea;
  ov->getChild("infoTower/infoContainer")
      ->getChild("infoTower/splashValue")
      ->setCaption(str.str());
  str.str("");
  str << _damage;
  ov->getChild("infoTower/infoContainer")
      ->getChild("infoTower/damageValue")
      ->setCaption(str.str());
  if(hasNextLevel){
    ov->getChild("infoTower/infoContainer")->getChild("infoTower/upgradeButton")->show();
  }
  else{
        ov->getChild("infoTower/infoContainer")->getChild("infoTower/upgradeButton")->hide();
  }

  ov->show();
}
}

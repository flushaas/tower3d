#include "OgrePlatform.h"
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
#include "gtest/gtest.h"
#include "graphml_boost.h"
#include <boost/graph/graphml.hpp>
#include <exception>

TEST(GRAPHTESTCASE, GRAPHLOADTEST) {
  graphml_boost* testGraph = new graphml_boost();
  try {
    ifstream _ifstream;
    _ifstream.open("/home/flush/PFC/TOWER3D/jni/test/level.xml",
                   std::ifstream::in);  // Abrimos el fichero para pasárselo a
                                        // read_graphml
    testGraph->loadGraph(&_ifstream);
    // test
    EXPECT_EQ(868, boost::num_vertices(testGraph->getGraph()))
        << "El n�mero de nodos no coincide" << std::endl;
    EXPECT_EQ(837, testGraph->getVertices("terrain").size())
        << "El n�mero de tiles de tipo terrain no coincide " << std::endl;
    EXPECT_EQ(31, testGraph->getVertices("road").size())
        << "El n�mero de tiles de tipo road no coincide" << std::endl;

  } catch (std::exception& se) {
    FAIL() << se.what() << std::endl;
  }
}
#endif

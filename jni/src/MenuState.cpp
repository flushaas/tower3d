/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/



#include "MenuState.h"
#include "InputHandler.h"
#include "towerCommon.h"
#include "PlayState.h"
#include "OgreOverlayManager.h"
#include "OgreOverlayContainer.h"
#include "OgreUtil.h"

#define PLAY_BUTTON_NAME "menu/Play"
#define SETTINGS_BUTTON_NAME "menu/Settings"
#define RECORDS_BUTTON_NAME "menu/Records"
#define CLOSE_BUTTON_NAME "menu/Exit"
namespace Tower3d{

  

  void MenuState::enter() {
      OverlayManager *mg = OverlayManager::getSingletonPtr();
      Overlay *ov = mg->getByName("Tower3d/menuOverlay");



      ov->show();
      ov->getChild("records/infoContainer")->hide();



  }
  
  void MenuState::exit() {

  }
  void MenuState::pause() {
    OverlayManager *mg = OverlayManager::getSingletonPtr();
    Overlay *ov = mg->getByName("Tower3d/menuOverlay");

    ov->hide();


  }
  void MenuState::resume(){
    OverlayManager *mg = OverlayManager::getSingletonPtr();
    Overlay *ov = mg->getByName("Tower3d/menuOverlay");
    ov->show();

    
  }
  bool  MenuState::frameStarted(const Ogre::FrameEvent &evt) {
    return _keepLiving;

    

  }
  bool MenuState::keyPressed(const SDL_Event *e) {

  if (e->key.keysym.sym == SDLK_ESCAPE) {
    _keepLiving = false;

  }

  return true;
}

  bool MenuState::mousePressed(const SDL_Event *e)

{

 
  Vector2 pos =  OgreUtil::getCursorPosition(e);
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/menuOverlay");
  OverlayElement *ovElement =
      ov->findElementAt(pos.x,pos.y);
  if (ovElement) {
    if(ovElement->getName()== PLAY_BUTTON_NAME ){
      pushState( new Tower3d::PlayState());
    }
    else     if(ovElement->getName()== CLOSE_BUTTON_NAME ){
      _keepLiving = false;
    }
    else     if(ovElement->getName()== RECORDS_BUTTON_NAME ){
            ov->getChild("records/infoContainer")->show();
      
    }
        else     if(ovElement->getName()== "records/upgradeButton" ){
	              ov->getChild("records/infoContainer")->hide();
	}

  } 
  return true;
}

  bool MenuState::frameEnded(const Ogre::FrameEvent &evt){


  }

    void MenuState::handleInputEvent(const SDL_Event *event){

    switch(event->type)
     { 
      case SDL_KEYDOWN:
	  keyPressed(event);
	  break;
     case SDL_MOUSEBUTTONDOWN:
	  mousePressed(event);
	  break;
     case SDL_FINGERDOWN:
       	  mousePressed(event);

     }



  }

}


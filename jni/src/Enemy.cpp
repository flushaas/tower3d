#include "Enemy.h"
#include "towerCommon.h"
#include <ctime>
#define RUN_ANIMATION "Run"
namespace Tower3d {

/**
 * Update enemy position, recalculates new directions if needed
 */
bool Enemy::update(double deltaTime) {
  if (!_active && time(NULL) > _time) {

    activate();
  }
  else if (_active) {


   _anim->addTime(deltaTime*2);

    Vector3 direction = _destiny - _node->getPosition();
    direction.normalise();
    direction *= (deltaTime * _speed);
    _node->setPosition(_node->getPosition() + direction);
    if (_node->getPosition().distance(_destiny) < MAX_ERROR_POSITION) {
      // NEXT WAYPOINT REACHED, CHANGE DESTINY

      _idxTargetNode--;
      if(_idxTargetNode<0){
	return true;
      }
      else{
	  tile_props destinyPos = _route[_idxTargetNode];
	  _destiny = Vector3(destinyPos.x,destinyPos.y,destinyPos.z)+Ogre::Vector3(0,1,0);

      }

    }
  }
  return false;
}
/**
 * Activate an enemy, drawing it in the map
 */
void Enemy::activate() {

  _active = true;
  tile_props startPos = _route.back();
  tile_props destinyPos = _route[_route.size()-2];
  _idxTargetNode = _route.size()-2;
  Ogre::SceneManager *scnMgr =
      Ogre::Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);
  Entity *ent = scnMgr->createEntity(_meshName);
  this->_node = scnMgr->createSceneNode(_id);
  try{
    this->_node->attachObject(
			      scnMgr->createParticleSystem(_id, _type + "/Smoke"));
  }
  catch(std::exception e){
    //Nothing to do
  }
  this->_node->attachObject(ent);
  this->_node->scale(0.3, 0.3, 0.3);
  this->_node->yaw(Ogre::Radian(Ogre::Degree(_rotateY)));
  this->_node->setPosition(Ogre::Vector3(startPos.x,startPos.y,startPos.z)+Ogre::Vector3(0,1,0));
  scnMgr->getRootSceneNode()->addChild(this->_node);
  _destiny = Vector3(destinyPos.x,destinyPos.y,destinyPos.z)+Ogre::Vector3(0,1,0);
  _anim = ent->getAnimationState(RUN_ANIMATION);
   _anim->setEnabled(true);
  _anim->setLoop(true);
  _anim->setTimePosition(0.0);

}
}

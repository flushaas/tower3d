#include "MapManager.h"
#include "OgreResource.h"
#include "OgreResourceGroupManager.h"
#include "Map.h"
#include "towerCommon.h"

template <>
Tower3d::MapManager *Ogre::Singleton<Tower3d::MapManager>::msSingleton = 0;
namespace Tower3d {

MapManager *MapManager::getSingletonPtr() { return msSingleton; }

MapManager &MapManager::getSingleton() {
  assert(msSingleton);
  return (*msSingleton);
}

MapManager::MapManager() {  // Scripting is supported by this manager
  mScriptPatterns.push_back("*.mapdef");
  // Register scripting with resource group manager
  Ogre::ResourceGroupManager::getSingleton()._registerScriptLoader(this);

  // Resource type

  mResourceType = "Map";

  // low, because it will likely reference other resources
  mLoadOrder = 30.0f;

  // this is how we register the ResourceManager with OGRE
  Ogre::ResourceGroupManager::getSingleton()._registerResourceManager(
      mResourceType, this);
}

MapManager::~MapManager() {
  // and this is how we unregister it
  Ogre::ResourceGroupManager::getSingleton()._unregisterResourceManager(
      mResourceType);
}

MapPtr MapManager::load(const Ogre::String &name, const Ogre::String &group) {
  MapPtr map = getResourceByName(name).staticCast<Tower3d::Map>();

  if (map.isNull())
    map = createResource(name, group).staticCast<Tower3d::Map>();

  map->load();
  return map;
}

Ogre::Resource *MapManager::createImpl(
    const Ogre::String &name, Ogre::ResourceHandle handle,
    const Ogre::String &group, bool isManual,
    Ogre::ManualResourceLoader *loader,
    const Ogre::NameValuePairList *createParams) {
  return new Map(this, name, handle, group, isManual, loader);
}
}

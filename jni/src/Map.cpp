#include "Map.h"
#include "graphml_boost.h"
#include "OgreSceneNode.h"
#include "OgreRoot.h"
#include "towerCommon.h"
#include "OgreUtil.h"
#include "OgreStaticGeometry.h"
#include <sstream>

namespace Tower3d {

/**
 *Constructor as Resource
 */
Map::Map(Ogre::ResourceManager *creator, const Ogre::String &name,
         Ogre::ResourceHandle handle, const Ogre::String &group, bool isManual,
         Ogre::ManualResourceLoader *loader)
    : Ogre::Resource(creator, name, handle, group, isManual, loader) {
  createParamDictionary("Map");
  _meshName = name.substr(0,name.length()-7)+".mesh";
 
}
/**
 * Destroy all resources
 */
  Map::~Map() { unload(); }
/**
 * Load the resource Map Level.xml and build all the map tiles from it
 */
void Map::loadImpl() {
  Ogre::DataStreamPtr stream =
      Ogre::ResourceGroupManager::getSingleton().openResource(mName, mGroup,
                                                              true, this);





  

  std::stringstream istream;
  istream << stream->getAsString();

  graphml_boost *graph = new graphml_boost();
  graph->loadGraph(&istream);

  Ogre::SceneManager *scnMgr =
      Ogre::Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);

  Ogre::StaticGeometry *mapSG =   scnMgr->createStaticGeometry(_meshName+"SG");
  Entity * ent = scnMgr->createEntity(_meshName);
  mapSG->addEntity(ent,Vector3(0,0,0));
  mapSG->build();

  this->_graphml = graph;

  //ruta_t vertices = graph->getVertices(ALL_TYPES);
  int pos = 0;
  /*  std::for_each(vertices.begin(), vertices.end(),
                [&pos, this](tile_props &prop) -> void {
                  apTile *tile = new MapTile(prop, pos);
                  this->_tiles.push_back(std::shared_ptr<MapTile>(tile));
                  tile->draw();
                  pos++;
		  });*/
}

/**
 * to be executed when unloading the resource.
 */
void Map::unloadImpl() {
    Ogre::SceneManager *scnMgr =
      Ogre::Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);
    scnMgr->destroyStaticGeometry(_meshName+"SG");

  if (_graphml != NULL) {
    delete _graphml;
    _graphml = NULL;
  }
  _tiles.clear();
}

/**
 * Calculate the size of the level
 */
size_t Map::calculateSize() const { return 1000; }

  Ogre::Vector3 Map::getStartPosition() {
    tile_props tile = _graphml->getVertices(startTile)[0];
    return Ogre::Vector3(tile.x, tile.y, tile.z);
  }
  Ogre::Vector3 Map::getEndPosition() {
    tile_props tile = _graphml->getVertices(endTile)[0];
    return Ogre::Vector3(tile.x, tile.y, tile.z);
  }
  ruta_t Map::getRoute(Ogre::Vector3 position) {
     tile_props node= _graphml->getCurrentNode(position);
    _graphml->calculateRouteBFS(std::atoi(node.id.c_str()));
    ruta_t aux=  _graphml->getRuta(std::atoi(node.id.c_str()),std::atoi(_graphml->getVertices(endTile)[0].id.c_str()));


    return aux;
	


  }
  Ogre::Vector3 Map::getNextWayPoint(Ogre::Vector3 position) {

    tile_props node= _graphml->getCurrentNode(position);
    _graphml->calculateRouteBFS(std::atoi(node.id.c_str()));

    ruta_t route =  getRoute(position);
	
    tile_props nextWayPoint= (route.size()>1 ? route.at(route.size()-2) : route.at(0));
    return Ogre::Vector3(nextWayPoint.x,nextWayPoint.y,nextWayPoint.z);
    



  }

  bool Map::containsRoad(Ogre::AxisAlignedBox aaBox){

   ruta_t route = _graphml->getVertices(road);


   for(int i = 0 ;i < route.size();i++){
     if(aaBox.contains(Vector3(route[i].x, route[i].y, route[i].z))){
       return true;
     }
   }
   return false;
  }
}

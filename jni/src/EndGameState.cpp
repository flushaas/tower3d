/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/



#include "MenuState.h"
#include "EndGameState.h"
#include "InputHandler.h"
#include "towerCommon.h"
#include "MenuState.h"
#include "OgreOverlayManager.h"
#include "OgreOverlayElement.h"
#include "OgreOverlayContainer.h"
#include "OgreUtil.h"


namespace Tower3d{

  

  void EndGameState::enter() {

     SDL_StartTextInput();
      OverlayManager *mg = OverlayManager::getSingletonPtr();
      Overlay *ov = mg->getByName("Tower3d/EndGame");
      
      ov->getChild("endGame/Container")->getChild("text/winLose")->setCaption(_win? "You Win!":"You Loose!");
      stringstream str;
      str << _score;
      ov->getChild("endGame/Container")->getChild("text/endGameScore")->setCaption(str.str());
      ov->show();



  }
  
  void EndGameState::exit() {
    
      OverlayManager *mg = OverlayManager::getSingletonPtr();
      mg->getByName("Tower3d/EndGame")->hide();
      std::cout << "Limpiando" << std::endl;
      SDL_StopTextInput();
  }
  void EndGameState::pause() {
    SDL_StopTextInput();
    OverlayManager *mg = OverlayManager::getSingletonPtr();
    Overlay *ov = mg->getByName("Tower3d/EndGame");

    ov->hide();


  }
  void EndGameState::resume(){
    SDL_StartTextInput();
    OverlayManager *mg = OverlayManager::getSingletonPtr();
    Overlay *ov = mg->getByName("Tower3d/EndGame");
    ov->show();

    
  }
  bool  EndGameState::frameStarted(const Ogre::FrameEvent &evt) {


    

  }
bool EndGameState::keyPressed(const SDL_Event *e) {
  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/EndGame");
  stringstream str;
  str << ov->getChild("endGame/Container")->getChild("text/RecordName")->getCaption();
  
    ov->getChild("endGame/Container")->getChild("text/RecordName")->setCaption(str.str());
  


}

  bool EndGameState::mousePressed(const SDL_Event *e)



  {

    std::cout << "Raton pulsado" << std::endl;
    Vector2 pos = OgreUtil::getCursorPosition(e);

  OverlayManager *mg = OverlayManager::getSingletonPtr();
  Overlay *ov = mg->getByName("Tower3d/EndGame");
  OverlayElement *ovElement = ov->findElementAt(pos.x, pos.y);
  
  if(ovElement){
    std::cout << "Elemento " << ovElement->getName() <<std::endl;
    if(ovElement->getName() == "endGame/Confirm"){
      popState();


    }
  }


 

}

  bool EndGameState::frameEnded(const Ogre::FrameEvent &evt){


  }

    void EndGameState::handleInputEvent(const SDL_Event *event){

    switch(event->type)
     {
       case SDL_TEXTINPUT:
	 {
	  OverlayManager *mg = OverlayManager::getSingletonPtr();
	  Overlay *ov = mg->getByName("Tower3d/EndGame");
	  stringstream str;
	  str << mg->getOverlayElement("text/RecordName")->getCaption() << event->text.text;
	    mg->getOverlayElement("text/RecordName")->setCaption(str.str());
	  break;
	 }
     case SDL_MOUSEBUTTONDOWN:
       {
	  mousePressed(event);
	  break;
       }
     case SDL_FINGERDOWN:
       {
       	  mousePressed(event);
	  break;
       }

     }



  }

}


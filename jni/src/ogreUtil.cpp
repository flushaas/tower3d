#include "OgreUtil.h"
#include "OgreSceneNode.h"
#include "OgreMesh.h"
#include "OgreVector3.h"

using namespace Ogre;

Ogre::Vector2 OgreUtil::getCursorPosition(const SDL_Event *e) {

  Ogre::Viewport *viewport = Ogre::Root::getSingletonPtr()
                                 ->getRenderTarget("OgreWindow")
                                 ->getViewport(0);
  double width = viewport->getActualWidth();

  double height = viewport->getActualHeight();

  float x, y;

  if (e->type == SDL_MOUSEMOTION || e->type == SDL_BUTTON_LEFT ||
      e->type == SDL_MOUSEBUTTONDOWN) {
    x = e->motion.x / width;
    y = e->motion.y / height;
  } else if (e->type == SDL_FINGERUP || e->type == SDL_FINGERDOWN) {
    x = e->tfinger.x;
    y = e->tfinger.y;
  }

  return Ogre::Vector2(x, y);
}

void OgreUtil::destroyAllAttachedMovableObjects(SceneNode *node) {
  if (!node)
    return;

  // Destroy all the attached objects
  SceneNode::ObjectIterator itObject = node->getAttachedObjectIterator();

  while (itObject.hasMoreElements())
    try {

      node->getCreator()->destroyMovableObject(itObject.getNext());
    } catch (...) {
      // Empty catch, to skip OgreBullet trying to delete
    }

  // Recurse to child SceneNodes
  SceneNode::ChildNodeIterator itChild = node->getChildIterator();

  while (itChild.hasMoreElements()) {
    SceneNode *pChildNode = static_cast<SceneNode *>(itChild.getNext());
    destroyAllAttachedMovableObjects(pChildNode);
  }
}

void OgreUtil::destroySceneNode(Ogre::SceneNode *node) {
  if (!node)
    return;
  destroyAllAttachedMovableObjects(node);
  node->removeAndDestroyAllChildren();
  node->getCreator()->destroySceneNode(node);
}

bool OgreUtil::projectPos(Camera *cam, const Ogre::Vector3 &pos, Ogre::Real &x,
                          Ogre::Real &y) {
  Vector3 eyeSpacePos = cam->getViewMatrix(true) * pos;
  // z < 0 means in front of cam
  if (eyeSpacePos.z < 0) {
    // calculate projected pos
    Vector3 screenSpacePos = cam->getProjectionMatrix() * eyeSpacePos;
    x = (screenSpacePos.x+1)/2;
    y = (-screenSpacePos.y+1)/2;
    return true;
  } else {
    x = (-eyeSpacePos.x > 0) ? -1 : 1;
    y = (-eyeSpacePos.y > 0) ? -1 : 1;
    return false;
  }
}


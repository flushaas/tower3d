#ifndef MAP_MANAGER_H
#define MAP_MANAGER_H
#include "OgreResourceManager.h"
#include "OgreStringVector.h"

#include "Map.h"

namespace Tower3d {
/**
 * Resource Manger for maps. Read Map from Xml files
 */
class MapManager : public Ogre::ResourceManager,
                   public Ogre::Singleton<MapManager> {
 protected:
  /**
   * Constructor

   * @param name Name of the resource file
   * @param handle Resource handle
   * @param group Resource group
   * @param isManual true if resource is manually loaded
   * @param loader Resource loader if its manually loaded
   * @param createParams parameters
   */
  Ogre::Resource *createImpl(const Ogre::String &name,
                             Ogre::ResourceHandle handle,
                             const Ogre::String &group, bool isManual,
                             Ogre::ManualResourceLoader *loader,
                             const Ogre::NameValuePairList *createParams);

 public:
  /**
   * Constructor. Load Map Definitios 
   */
  MapManager();

  /**
   * Destructor
   */
  virtual ~MapManager();

  /**
   * Load a Map fromn a definition
   * @param name name of the resource
   * @param group Resource group name
   */
  virtual MapPtr load(const Ogre::String &name, const Ogre::String &group);

  /**
   * Get singleton
   */
  static MapManager &getSingleton();

  /**
   * Get Singleton pointer
   */
  static MapManager *getSingletonPtr();
};
}
#endif

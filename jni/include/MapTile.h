#ifndef MAPTILE_H
#define MAPTILE_H
#include "graphTypes.h"
#include "TileType.h"
#include "OgreVector3.h"
#include "OgreSceneNode.h"
#include <string>
#include <sstream>

#define terreno "terrain"
#define road "road"
#define startTile "start"
#define endTile "end"

namespace Tower3d {

/**
 * @class MapTile. Represents a tile of the map.
 */

class MapTile {
  /**
   * Tile properties
   */

  tile_props _props;

  /**
   * OgreScene Node where the Tile is Draw
   */
  Ogre::SceneNode *_node;
  std::string _id;

 public:
  /**
   * Constructor.
   * @param tile_props Properties of the tile, like position and type.
   * @param id Numeric identifier.
   */
  inline MapTile(tile_props props, int id) {
    std::stringstream str;
    str << "Tile" << props.type << id; 
    _props = props;
    _id = str.str();
  }
  /**
   * Return type of the tile
   */
  TileType inline getType() {
    if (_props.type == std::string(terreno)) {
      return TileType::TERRAIN;
    } else if (_props.type == std::string(startTile)){
      return TileType::START;
    } else if (_props.type == std::string(endTile)){
      return TileType::END;
    }
    else{
      return TileType::ROAD;
    }

  };

  void draw();

  /*
   * Return the position  of the tile
   */
  Ogre::Vector3 inline getPosition() {
    return Ogre::Vector3(_props.x, _props.y, _props.z);
  };
};
}
#endif

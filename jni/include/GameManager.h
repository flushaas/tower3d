/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GameManager_H
#define GameManager_H

#include <stack>
#include <OgreSingleton.h>


#include "InputManager.h"
#include "MapManager.h"


class GameState; 

/**
 * @brief Controls game flush, changing from one state to another. Also redirect input to current State
 */
class GameManager : public Ogre::FrameListener,
                    public Ogre::Singleton<GameManager>,
		    public Tower3d::InputHandler


{
 public:
  /**
   * @brief Default Constructor
   */
  GameManager();

  /**
   * @brief Default Destructor
   */
  ~GameManager(); 



  /**
   * @brief Start the game, giving control to a state
   * @param state State that takes control 
   */
  void start(GameState* state);

  /**
   * @brief Change current state to new one. The old current state is deleted
   * @param state State that takes control
   */
  void changeState(GameState* state);
  
  /**
   * @brief Change current state to new one. The old current state is paused
   * @param state State that takes control
   */
  void pushState(GameState* state);

  /**
   * @brief Remove current State from stack, and gives control to the previous state
   */
  void popState();


  /**
   * @brief Neeed for Ogre::Singlenton
   */
  static GameManager& getSingleton();

  /**
   * @brief Neeed for Ogre::SinglentonPtr
   */
  static GameManager* getSingletonPtr();

  /**
   * @brief This method is called when a Input event is throwed.
   * @param event Input Event throwed by SDL
   */
  virtual void handleInputEvent(const SDL_Event *event);


 protected:

  /**
   * @brief Ogre Root Object
   */
  Ogre::Root* _root;

  /**
   * @brief Main scene Manager
   */
  Ogre::SceneManager* _sceneMgr;

  /**
   * @brief Current Window
   */
  Ogre::RenderWindow* _renderWindow;


  /**
   * @brief Load all resource locations, readed from resources.cfg or resources_android.cfg, if compiled for ANDROID platform.
   */
  void loadResources();

  /**
   * @brief In linux, restore config or show the config dialog if no config found. In Android, manual install GLES plugin, OCTREE plugin and ParticleFX plugin.
   */
  bool configure();


  /**
   * @brief Executed every time Frame is Started. It calls current State FrameStarted.
   * @param evt FrameEvent.
   */
  bool frameStarted(const Ogre::FrameEvent& evt);

  /**
   * @brief Executed every time frame is ended. It calls current State FrameEnded.
   */
  bool frameEnded(const Ogre::FrameEvent& evt);


 private:

  /**
   * @brief Input Manager that get inputs event.
   */
  Tower3d::InputManager* _inputMgr;

  /**
   * @brief Stack of states. Current State is in top of the stack.
   */
  std::stack<GameState*> _states;

  /**
   * @brief  Map Manager. Loads maps configuration.
   */
  Tower3d::MapManager* _mapManager;

  /**
   * @brief window created by SDL
   */
  SDL_Window *window ;

  /**
   * @brief Shows the loading Image using SDL
   */
  void showLoadingImage();
  #if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

  /**
   * @brief Plugin list. Needed to unload where destroying.
   */
  std::vector<Ogre::Plugin*> _plugins;
  #endif
};

#endif

#ifndef MAP_H
#define MAP_H
#include <string>
#include <vector>
#include <memory>
#include "MapTile.h"
#include "OgreResource.h"
#include "OgreResourceManager.h"
#include "OgreString.h"
#include "OgreSceneNode.h"

class graphml_boost;

namespace Tower3d {

/**
 * Represent a game level of the Tower Defense
 */
class Map : public Ogre::Resource {
  /**
   * private constructor, initilize vector of tiles
   */
  inline Map(){};

protected:
  /**
   * Method executed by its resource manager to load the resource
   */
  void loadImpl();
  /**
   * Method executed by its resource manager to unload the resource
   */
  void unloadImpl();

  /**
   * Needs to implements. Return 1000 as fixed value
   */
  size_t calculateSize() const;

  /**
   * Vector of tiles
   */
  std::vector<std::shared_ptr<MapTile>> _tiles;

  /**
   * graphml Boost
   */
  graphml_boost *_graphml;
  
  /**
   * meshName for the map. It will be loaded as Static Geometry
   */
  std::string _meshName;
  /**
 * Load a Map from a GraphML file.
 * @param fileName. File Name of the graphml File
 */

public:
  /**
   * Constructor
   * @param creator  pointer to the MapManager
   * @param name Name of the resource file
   * @param handle Resource handle
   * @param group Resource group
   * @param isManual true if resource is manually loaded
   * @param loader Resource loader if its manually loaded
   */
  Map(Ogre::ResourceManager *creator, const Ogre::String &name,
      Ogre::ResourceHandle handle, const Ogre::String &group,
      bool isManual = false, Ogre::ManualResourceLoader *loader = 0);

  /**
   * Default Destructor.
   * Delete geometry and graph info
   */
  virtual ~Map();

  /**
   * Get the position of the Start place
   */
  Ogre::Vector3 getStartPosition();

  /**
   * Get the position of the End place
   */
  Ogre::Vector3 getEndPosition();

  /**
   * Get route to the end from a position
   * @param position to do the pathfinding
   */
  ruta_t getRoute(Ogre::Vector3 position);

  /**
   * Get next WayPoint to reach the end
   * @param position to do the pathFinding
   */
  Ogre::Vector3 getNextWayPoint(Ogre::Vector3 position);

  /**
   * check if the Bounding Box Contains a road vertex
   * @param aaBox Bounding Box to check
   */
  bool containsRoad(Ogre::AxisAlignedBox aaBox);
   
};

  /**
   * Typedef needed by Resource inheritance
   */  
typedef Ogre::SharedPtr<Map> MapPtr;
}
#endif

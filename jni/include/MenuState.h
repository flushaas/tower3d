/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef MenuState_H
#define MenuState_H

#include "GameManager.h"
#include "InputHandler.h"
#include "GameState.h"
namespace Tower3d{
// Clase abstracta de estado básico.
// Definición base sobre la que extender
// los estados del juego.
class MenuState : public GameState {
 public:
  MenuState():_keepLiving(true) {}

  // Gestión básica del estado.nnn
  virtual void enter() ;
  virtual void exit() ;
  virtual void pause() ;
  virtual void resume();

  // Gestión básica para el tratamiento
  // de eventos de teclado y ratón.

  void handleInputEvent(const SDL_Event *event);
  bool keyPressed(const SDL_Event *e) ;
  bool mousePressed(const SDL_Event *e);
  virtual bool frameStarted(const Ogre::FrameEvent &evt) ;
  virtual bool frameEnded(const Ogre::FrameEvent &evt) ;

  // Gestión básica de transiciones.
protected:
  bool _keepLiving;

};
}
#endif

#ifndef OGREUTIL_H
#define OGREUTIL_H
#include "OgreSceneNode.h"
#include "OgreMesh.h"
#include "OgreVector3.h"
#include "OgreSceneManager.h"
#include "OgreSubMesh.h"
#include "OgreRoot.h"
#include "SDL.h"

using namespace Ogre;


/**
 * @brief Several Ogre Utils
 */
class OgreUtil {
 public:
  /**
   * @brief Destroy All objects attached to a SceneNode
   * @param node Node to destroy all atached objects to.
   */
  static void destroyAllAttachedMovableObjects(Ogre::SceneNode* node);

  /**
   * @brief destroy a Scene node and all atached objects.
   * @param node Node to destroy
   */
  static void destroySceneNode(Ogre::SceneNode* node);

  /**
   * @brief Get Cursor or finger position in relatives coords [0,1]
   * @e Event with the curor/finger info
   */
  static Ogre::Vector2 getCursorPosition(const SDL_Event *e);

  /**
   * @brief Get Projected 2D position in viewPort from a 3D position.
   * @param cam Camera that creates the viewport.
   * @param pos 3D position to project.
   * @param x reference to store X coodinate.
   * @param y reference to store y coordinate.
   */
  static bool projectPos(Camera *cam, const Ogre::Vector3 &pos, Ogre::Real &x,Ogre::Real &y);

  


};

#endif

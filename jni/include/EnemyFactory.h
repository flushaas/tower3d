#ifndef ENEMY_FACTORY_H
#define ENEMY_FACTORY_H
#include <map>
#include "Map.h"
#include <string>
#include <memory>
#include "Enemy.h"


#define MONOCYCLE_ENEMTY "Monocycle"



namespace Tower3d {

/*
 * Factory Pattern Class that loads the enemy definition from
 * enemys.conf
 */
class EnemyFactory {
 protected:
  /**
   * List of enemy definition 
   */
  std::map<std::string, std::map<std::string, std::string>> _enemyDefs;

  /**
   * Map where is placed de enemy
   */
  MapPtr _map;

  /**
   * Route to follow through the map
   */
  ruta_t _route;

 public:
  /**
   * Constructor. Reads enemys.cfg and loads all the enemys definition.
   * @param map Map where the EnemyFactory are generated
   */
  EnemyFactory(MapPtr map);

  /**
   * Create a Enemy of a type.
   * @param enemyType Type of the enemy
   * @param id Unique id of the enemy
   * @param time Time when the enemy should appear in the map 
   */
  std::shared_ptr<Enemy> createEnemy(std::string enemyType, std::string id, long time);
};
}
#endif

#ifndef TOWER_H
#define TOWER_H
#include <string>
#include "OgreSceneNode.h"
#include "OgreRoot.h"
#include "OgreSceneManager.h"
#include "towerCommon.h"
#include "OgreParticleSystem.h"
#include "OgreEntity.h"
#include "Sound.h"
#include "OgreParticleSystem.h"
#include "OgreRingEmitter.h"
#include <memory>
#include "OgreUtil.h"
#include "OgreBillboardSet.h"

#define TOLERANCE_DEGREES 2

using namespace Ogre;
using namespace std;

namespace Tower3d {

class Projectile;
class Enemy;
  /**
   * This class represents a defensive tower of the game. It's at charge of enemy tracking and shooting.
   */
class Tower {
protected:
  /**
   * Unique id for the Tower. It starts with the "tower" string.
   */
  std::string _id;

  /**
   * Tower Range of attack
   */
  float _range;

  /**
   * Time between two shoots
   */
  float _timeReload;

 
  /**
   * Damage made to the enemies
   */
  float _damage;

  /**
   * Splash Area. Area around the point of impact of the projectiles throwed by this tower. Every enemy
   * within this area suffers damage.
   */
  float _splashArea;
  /**
   * Scene Node of the Tower. See class description for more info.
   */
  Ogre::SceneNode *_node;

  /**
   * Mesh Name of the Tower
   */
  std::string _meshName;

  /**
   * Projectile Mesh Name
   */
  std::string _projectileMeshName;
 /**
   * Point of the tower where the projectile started. (Usually the cannon)
   */
  Vector3 _shootOrigin;
  /**
   * How fast the projectile travel
   */
  float _projectileSpeed;

 



  /**
   * It stored the destiny of the shoot (normally matchs enemy position)
   */
  Vector3 _shootDestiny;

  /**
   * Rotation Speed of the tower when trackin enemies.
   */
  float _rotationSpeed;

    /**
   * Intial offset of the Tower. Usually needed if the center of the tower is not in the base.
   */
  Vector3 _initialOffset;

  /**
   * Initial Scale applied to the Tower. To adapt diferent mesh sizes.
   */
  Vector3 _initialScale;

  /**
   * Enemy that is tracked by the tower.
   */
  shared_ptr<Enemy> _targetedEnemy;

  /**
   * Billboard to represent Tower Range. It wil change color dependnig of _validPosition field
   */
  BillboardSet *bset;


  /**
   * Vector of projectiles shooted by the tower that still are travelling to its target.
   */
  std::vector<std::shared_ptr<Projectile>> _activeProjectiles;
 /**
   * It stores the time fo the last shoot made by this tower. Needed to check if the reloading time is over
   */
  float _timeSinceLastShoot;

  /**
   * Number of projectiles throwed by this tower. Needed for providing unique identifer to each projectile throwed.
   */
  int _projectileCount;
    /**
   * Only needed when building a tower. True if the tower can be builded in the current position. False in othercase.
   */
  bool _validPosition;

  /**
   * Price of Building the Tower
   */
  float _price;

  /**
   * Type of the Tower
   */
  std::string _type;

  /**
   * Level of the Tower
   */
  int _level;
  /**
   * Make a shoot against an enemyPosition
   * @param enemyPosition Target position to get Shooted
   */

  void shoot();

public:
  /**
   * Sound to play when the tower Shoots.
   */
  Sound *_shootSound;
  /**
   * Destructor. Destroy all ogre info of this tower.
   */
  inline ~Tower() { OgreUtil::destroySceneNode(_node->getParentSceneNode()); };

  /**
   * Constructor. See attributes for more info.
   */
  Tower(std::string id, float range, float timeReload, float damage,
        float splashArea, std::string meshName, std::string projectileMeshName,
        Vector3 shootOrigin, float projectileSpeed, std::string shootSound,
        float rotationSpeed, Vector3 initialOffset, Vector3 initialScale, float price, std::string type, int level=0)
      : _id(id), _range(range), _timeReload(timeReload), _damage(damage),
        _splashArea(splashArea), _meshName(meshName),
        _projectileMeshName(projectileMeshName), _shootOrigin(shootOrigin),
	_projectileSpeed(projectileSpeed), _rotationSpeed(rotationSpeed),
	_initialOffset(initialOffset), _initialScale(initialScale),
        _timeSinceLastShoot(0), _projectileCount(0),
        _validPosition(true),_price(price),_type(type),_level(level)

  {
    //    if (!Tower::_shootSound->isInitialized()) {
    _shootSound = new Sound();
      _shootSound->setSound(shootSound);
      //    }
  };
  /**
   * Get the unique Id of the tower
   */
  inline std::string getId() { return _id; };

  /**
   * Get the range of the tower
   */
  inline float getRange() { return _range; };

  /**
   * Set if the tower is in a valid building position. This will change the range
   * target color.
   */
  void setValidPosition(bool valid);

  /**
   * Get the reload time of the tower
   */
  inline float getTimeReload() { return _timeReload; };

  /**
   * Get the Damage of the tower
   */
  inline float getDamage() { return _damage; };

  /**
   * Get the splashArea of the Tower
   */
  inline float getSplashArea() { return _splashArea; };

  /**
   * Get the Mesh Name 
   */
  inline std::string getMeshName() { return _meshName; };

  /**
   * Get the entity represents the tower mesh. Needed for collision detection.
   */
  inline Entity *getEntity() {
    return (Entity *)this->_node->getAttachedObject(_id);
  };

  /**
   * Set tower Position
   * @param position  Tower Position
   * @param applyInitOffset True if needed tu sum the initialOffset to new Position
   */
  inline void setPosition(Vector3 position,bool applyInitOffset=true) {
    if(applyInitOffset){
      position+=_initialOffset;
    }
    _node->getParentSceneNode()->setPosition(position);
  };

  /**
   * Return towerPosition
   */
  inline Vector3 getPosition() {
    return _node->getParentSceneNode()->getPosition();
  };


  /**
   * Hide the circle range
   */
  inline void hideRange() {
    bset->setVisible(false);
  } // delete _node->detachObject(_id + "range");}
  /**
   * Return the Tower SceneNode
   */
  inline SceneNode *getNode() { return _node; };
  /**
   * Shows a circle around the range
   */
  inline void showRange() {
    bset->setVisible(true);
  }
  /**
   * Get tower Type
   */
  inline std::string getType(){
    return _type;
  }

  /**
   * Get Tower Level
   */
  inline int getLevel(){
    return _level;

  }
  /**
   * Draws the Tower. Create one Scene Nodppe for the Tower and one for the
   * Shoot Origin. It is needed
   * to rotate with the tower.
   */
  void activate();

  /**
   * Check if an enemy is within the range ot the tower. If it is and the tower
   * is not reloading, the tower makes a shoot. 
   * @param EnemyPosition Position to shoot
   */
  void checkEnemyOnRange(shared_ptr<Enemy> enemyPosition, float deltaTime);

  /**
   * Check what projectiles Hits, and returns them in a vector.
   */
  std::vector<std::shared_ptr<Projectile>> updateAndCheckHits(float deltaTime);

  /**
   * Show Tower properties
   * @param hasNextLeve. True if the tower is upgradeable. If not, the upgrade button is not showed.
   */
  void showProperties(bool hasNextLevel);
};
}

#endif

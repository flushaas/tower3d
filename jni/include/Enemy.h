#ifndef ENEMY_H
#define ENEMY_H
#include <string>
#include "OgreSceneNode.h"
#include "OgreRoot.h"
#include "OgreSceneManager.h"
#include "towerCommon.h"
#include "OgreParticleSystem.h"
#include "OgreEntity.h"
#include "OgreVector3.h"
#include "OgreAnimationState.h"
#include "Map.h"
#include "Projectile.h"
#include "OgreUtil.h"


using namespace Ogre;

namespace Tower3d {

class Map;

  /**
   * @brief Stores all the info of each Enemy.
   */
class Enemy {
 public:

  /**
   * @brief Destructor. Destroy node and entitys
   */
  inline ~Enemy(){  OgreUtil::destroySceneNode(_node); };

  /**
   * @brief Constructor.
   * @param id Unique id of the enemy
   * @param speed Speed of the Enemy
   * @param life Life Points of the Enemy
   * @param meshName Mesh name that represents this enemy
   * @param time Time when this enemy appears in the map
   * @param route Route of 3D points that will follow the enemy
   * @param rotateY Initial Rotate of the enemy
   * @param type enemy Type
   * @param points Points earned for killing this enemy
   * @param money earned for killing this enemy
   */
  Enemy(std::string id, float speed,float life, std::string meshName, long time,ruta_t route,float rotateY,std::string type,float points, float money)
      : _id(id),
        _speed(speed),
	_life(life),
	_time(time),
	_route(route),
        _meshName(meshName),
	_rotateY(rotateY),
	_type(type),
	_points(points),
	_money(money)
  {


    
    _active = false;
  };
  /**
   * @brief return enemy unique id
   */
  inline std::string getId() { return _id; };

  /**
   * @brief return enemy speed
   */
  inline float getSpeed() { return _speed; };

  /**
   * @brief return enemy Life points
   */
  inline float getLife() { return _life;};

  /**
   * @brief return mesh Name
   */
  inline std::string getMeshName() { return _meshName; };

  /**
   * @brief return enemy Position
   */
  inline Vector3 getPosition() { return _node->getPosition();};

  /**
   * @brief return true if enemy is Active(on the map and not dead)
   */
  inline bool getActive(){return _active;};

  /**
   * @brief return SceneNode of the enemy
   */
  inline Ogre::SceneNode * getNode(){return _node;};

  /**
   * @brief return Points earnead for killing the enemy
   */
  inline float getPoints(){return _points;};

  /**
   * @brief return money earned for killing the enemy
   */
  inline float getMoney(){return _money;};

  /**
   * @brief update the Enemy position
   * @param deltaTime Time since last move
   */
  bool update(double deltaTime);

  /**
   * @brief activate de enemy, drawing him on the map
   */
  void activate();
  /**
   * @brief Hit this enemy, reducing his life points the damage amount
   * @param damage Number of life points to substract
   * @return true if the enemy is dead;
   */
  inline bool hit(float damage){

    _life-=damage;
    if(_life<=0){
      _active = false;
      return true;
    }
    else {
      return false;
    }
      


  }
  /**
   * Set enemy position to new Position
   * @param position new Positino of the enemy
   */
  inline void setPosition(Vector3 position) { _node->setPosition(position); };

 protected:
  /**
   * unique enemy Id
   */
  std::string _id;

  /**
   * speed of the Enemy
   */
  float _speed;

  /**
   * life points
   */
  float _life;
  /**
   * time when enemy appears
   */
  long _time;

  /**
   * true if enemy is active
   */
  bool _active;
  /**
   * Mesh Name 
   */
  std::string _meshName;
  /**
   * Route of points that make the way to follow
   */
  ruta_t _route;

  /**
   * current waypoint of destiny for the enemy
   */
  Vector3 _destiny;

  /**
   * Initial Rotate, to orientate de Enemy
   */
  float _rotateY;

  /**
   * Type of the enemy
   */
  std::string _type;

  /**
   * points earned for killing the enemy
   */
  float _points;
  /**
   * money earned for killing the enemy
   */
  float _money;

  /**
   * Scene node of the enemy
   */
  Ogre::SceneNode *_node;

  /**
   * Run Animation of the enemy
   */
  Ogre::AnimationState* _anim;


   int  _idxTargetNode;




};
}

#endif

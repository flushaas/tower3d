#ifndef PlayState_H
#define PlayState_H


#include <string>
#include "GameState.h"
#include "OgreUtil.h"
#include "Map.h"
#include "OgreOverlayElement.h"
#include "Tower.h"
#include "TowerFactory.h"
#include "EnemyFactory.h"
#include <memory>

#include "WaveManager.h"
#include "SDL.h"

class Projectile;

#define TIME_SHOWING_MESSAGE 5

using namespace Ogre;

namespace Tower3d {
class PlayState : public GameState {
 public:
  PlayState():_keepLiving(true),_lives(20),_money(100),_score(0) {}
  ~PlayState();

  void enter();
  void exit();
  void pause();
  void resume();

  bool keyPressed(const SDL_Event *e);
  bool keyReleased(const SDL_Event *e);

  bool mouseMoved(const SDL_Event *e);
  bool mousePressed(const SDL_Event *e);
  bool mouseReleased(const SDL_Event *e);

  void handleInputEvent(const SDL_Event *event);
  bool frameStarted(const Ogre::FrameEvent &evt);
  bool frameEnded(const Ogre::FrameEvent &evt);

  void testScene();

 protected:

  /**
   * Ogre Root
   */
  Ogre::Root *_root;

  /**
   * Main SceneManager
   */
  Ogre::SceneManager *_sceneMgr;

  /**
   * Main Viewport
   */
  Ogre::Viewport *_viewport;

  /**
   * Main Camera
   */
  Ogre::Camera *_camera;

  /**
   * delta time between frames
   */
  Ogre::Real _deltaT;
  /**
   * Map loaded
   */
  MapPtr _map;

  /**
   * Tower that is going to be builded
   */
  std::shared_ptr<Tower3d::Tower> _buildingTower;

  /**
   * Tower selected
   */
  std::shared_ptr<Tower3d::Tower> _selectedTower;
  /**
   * list of towers builded
   */
  std::vector<std::shared_ptr<Tower3d::Tower>> _towers;

  /**
   * Tower Factory reference
   */
  std::unique_ptr<Tower3d::TowerFactory> _towerFactory;

  /**
   * Enemy Factory reference
   */
  std::shared_ptr<Tower3d::EnemyFactory> _enemyFactory;

  /**
   * Wave Manager reference.
   */
  std::unique_ptr<Tower3d::WaveManager> _waveManager;

  /**
   * Ray Scene Query
   */
  Ogre::RaySceneQuery *_rayScnQuery;

  /**
   * Num of towers builded
   */
  int _numTowers;

  /**
   * if false, the state ends
   */
  bool _keepLiving;

  /**
   * lives of the player
   */
  int _lives;

  /**
   * money of the player
   */
  int _money;

  /**
   * player score
   */
  int _score;

  /**
   * Time since Last Message is Showed (to check if its time to hide it)
   */
    float _timeSinceLastMessage;


 private:
  /**
   * Create Scene Map
   */
  void createScene();

  /**
   * Create Light
   */
  void createLight();

  /**
   *  Draw HUD
   */
  void createHUD();

  /**
   * Calls when a Hud element is clicked.
   * @param element OverlayElement that is clicked
   * @param SDL_Event Event that store the click data
   */
  bool hudElementClicked(Ogre::OverlayElement *element,const SDL_Event *e);
  /**
   * Checks the hit of the projectiles thrown by the towers
   * @param deltaTime Time elapase since previous call
   */
  void checkTowersHits(float deltaTime);
  /*
   * Move the new Tower to a new position.
   * @param pos X,y coordinates from the mouse cursor or finger
   */
  void moveTower(Vector2 pos);



  /**
   * Update money,scores and lives in the HUD
   */
  void updateHUDInfo();
  /**
   * Checks if the new Tower is in valid position
   * @return true if new Tower position is valid, false in other case
   */
  bool checkValidNewTowerPosition();

  /**
   * Select tower clicked by the user, showing the tower Properties
   */
  void  selectTowerClicked(Vector2 pos);

  /**
   * Hide Tower Info panel
   */
  void hideTowerInfo();

  /**
   * Show a message to the player
   * Message to be showed
   */
  bool showMessage(string message);

  

};
}
#endif

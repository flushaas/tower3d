#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include "OgreSingleton.h"
#include "SDL.h"
#include "SDL_mixer.h"



namespace Tower3d {
  class Sound;
/**
 * Sound Manager. Plays Music and FX
 */
class SoundManager : public Ogre::Singleton<SoundManager> {
 public:
  /**
   * Default Constructor
   */
  SoundManager();

  /**
   * Default Destructor
   */
  virtual ~SoundManager();

  /**
   * Load sound by name in a Mix Chunk
   * @param sound name
   */
  Mix_Chunk* loadSound(std::string sound);

  /**
   * play a sound
   * @param sound Soudn to play
   */
  void playFX(Sound* sound);

  /**
   * play a music
   * @p�ram sound Musci to play
   */
  void playMusic(Sound* sound);

  /**
   * stop music
   */
  void stopMusic();



  /**
   * Singleton
   */  
  static SoundManager &getSingleton();

  /**
   * Singleton pointer
   */
  static SoundManager *getSingletonPtr();
};
}
#endif

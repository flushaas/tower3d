#ifndef PROJECTILE_H
#define PROJECTILE_H
#include "OgreUtil.h"
#include "OgreSceneNode.h"
#include "OgreRoot.h"
#include "OgreSceneManager.h"
#include "towerCommon.h"
#include <string>

using namespace Ogre;

namespace Tower3d {

  /**
   * Class represents a projectile throwed by a Tower
   */
class Projectile {

protected:
  /**
   * Scene node of the projectile
   */
  Ogre::SceneNode *_node;

  /**
   * Projectile Direction
   */
  Vector3 _dir;

  /**
   * Projectile destinty
   */
  Vector3 _destiny;

  /**
   * Projectile speed
   */
  float _speed;

  /**
   * Projectile unique id
   */
  std::string _id;

  /**
   * Projectile damage
   */
  float _damage;

  /**
   * Projectile splash Area.
   */
  float _splashArea;
  

  /**
   * Time for the projectile to hit the target
   */
  float _timeToTarget;

  /**
   * Time since projectile shooted
   */
  float _timeSinceShoot;

public:
  /**
   * Destroy node and all entitys attached
   */
  inline ~Projectile() {OgreUtil::destroySceneNode(_node); }

  /**
   * Constructor
   */
  inline Projectile(Vector3 origin, Vector3 destiny, float speed,
                    std::string meshName, std::string id, float damage, float splashArea)
    : _destiny(destiny), _speed(speed), _id(id), _damage(damage),
      _splashArea(splashArea),_timeSinceShoot(0) {

    Ogre::SceneManager *scnMgr =
        Ogre::Root::getSingletonPtr()->getSceneManager(MAIN_SCN_MGR);

    _node = scnMgr->createSceneNode(_id);
    Entity *entProjectile = scnMgr->createEntity(meshName);
    _node->attachObject(entProjectile);
    scnMgr->getRootSceneNode()->addChild(_node);
    _node->setPosition(origin);
    _dir = (destiny -origin);
    //_distance =  destiny.distance(origin);
    _timeToTarget = _dir.length()/_speed;

    _dir.normalise();

  }
  /**
   * Get Projectile Damage
   */
  inline float getDamage() { return _damage; }

  /**
   * Get Splash Areas
   */ 
  inline float getSplashArea(){ return _splashArea;}

  /**
   * Move the projectile and checks if the projectile reached its hitpoint
   * @param deltaTime time since last check
   * @return Returns true if the projectile hits its target(not neeed to hit an
   * enemy)
   */
  inline bool updateAndCheck(float deltaTime) {

	Ogre::Vector3 aux  = _dir *( _speed * deltaTime);
	_timeSinceShoot += deltaTime;

	_node->setPosition(_node->getPosition() + aux);

	//    if (_node->getPosition().distance(_destiny) > _distance) {
    if (_timeSinceShoot>=_timeToTarget) {

      return true;
    }

    return false;
  }

  inline Vector3 getDestiny(){return _destiny;};
};
}
#endif

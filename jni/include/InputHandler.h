#include "SDL.h"

#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H
namespace Tower3d{
/**
 * @brief Interface to all classes capable of manage SDL events
 */
class InputHandler{

public:
  /**
   * @brief  Method called to Handle Events
   * @param event Input Event.
   */
  virtual void handleInputEvent(const SDL_Event *event)=0;

};
}
#endif

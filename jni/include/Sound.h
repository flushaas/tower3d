#include "SDL.h"
#include "SDL_mixer.h"
#include "SoundManager.h"

namespace Tower3d {

/**
 * Represents a Sound. Needed to control the free of Chunks of SDL
 */
class Sound {

public:
  /**
   * Defaul Constructor. 
   * @param sound FileName of the sound to use
   */
  inline Sound(std::string sound) {
    setSound(sound);

  }
  /**
   * Default Constructor
   */
  inline Sound(){};

  /**
   * Check if the sound is initialized
   */
  inline bool isInitialized(){
    return _chunk;
  }

  /**
   * Load Sound by name
   * @param sound Sound name to load
   */
  inline void setSound(std::string sound){
      _chunk = SoundManager::getSingletonPtr()->loadSound( sound);
  }

  /**
   * Sound destructor. Free SDL sound chunk
   */
  inline ~Sound() { Mix_FreeChunk(_chunk); }

  /**
   *  return the SDL sound chunk
   */
  inline Mix_Chunk *getChunk() { return _chunk; };

private:

  /**
   * SDL sound chunk
   */
  Mix_Chunk* _chunk = NULL;
};
}

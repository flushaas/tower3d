#ifndef TOWER_COMMON_H
#define TOWER_COMMON_H
#include "OgrePlatform.h"
#include "OgreViewport.h"
#include "OgreVector2.h"
#include "OgreRoot.h"
#include "SDL.h"
#define MAX_ERROR_POSITION 0.1
#define MAIN_SCN_MGR "MAIN_SCN_MGR"
#define MAP_SCENE_NODE "MAP_SCEN_NODE"

/**
 * @brief Mask for querys
 */
enum QueryFlags {
  MAP_MASK = 1 << 0,
  
  TOWER_MASK = 1 << 2,

};

/**
 * @brief Reward for killing an Enemy
 */
typedef struct Reward {

  /**
   * @brief money earned for killing the enemy
   */
  int money;

  /**
   * @brief points earned for killing the enemy
   */
  int points;

}Reward;







#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID



#include <android/log.h>
#include <android/asset_manager.h>
/**
 * @brief AAssetManager from Android
 */
extern AAssetManager* assetMgr;


#endif
#endif

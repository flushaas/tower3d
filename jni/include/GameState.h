/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GameState_H
#define GameState_H



#include "GameManager.h"
#include "InputHandler.h"

/**
 * @brief Base Game State, with handles one Game Phase.
 */
class GameState : public Tower3d::InputHandler{
 public:
  /**
   * @brief Default Constructor
   */
  GameState() {}

  /**
   * @brief Executed once when state is inserted in the stack.
   */
  virtual void enter() = 0;
  /**
   * @brief Executed once when state pops from the stack.
   */
  virtual void exit() = 0;

  /**
   * @brief Executed once when the state is Paused
   */
  virtual void pause() = 0;

  /**
   * @brief Executed once when the state is resumed
   */
  virtual void resume() = 0;


  /**
   * @brief Executed when frame started.
   */
  virtual bool frameStarted(const Ogre::FrameEvent &evt) = 0;
  /**
   * @brief Executed when frame ended.
   */
  virtual bool frameEnded(const Ogre::FrameEvent &evt) = 0;

  /**
   * @brief Change current state to new one. The old current state is deleted
   * @param state State that takes control
   */

  void changeState(GameState *state) {
    GameManager::getSingletonPtr()->changeState(state);
  }
  /**
   * @brief Change current state to new one. The old current state is paused
   * @param state State that takes control
   */
  void pushState(GameState *state) {
    GameManager::getSingletonPtr()->pushState(state);
  }
  /**
   * @brief Remove current State from stack, and gives control to the previous state
   */
  void popState() { GameManager::getSingletonPtr()->popState(); }
};

#endif

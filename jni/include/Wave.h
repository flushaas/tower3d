#ifndef WAVE_H
#define WAVE_H

#include <map>
#include <string>
#include <vector>
#include <memory>
#include "EnemyFactory.h"
#include "Enemy.h"
#include "towerCommon.h"

#define TIME_OFFSET_END 15


namespace Tower3d{

  typedef std::map<std::string, std::string> t_waveProperties;
class Wave {

protected:

  /**
   * List of wave's enemies
   */
  std::vector<std::shared_ptr<Enemy>> _enemies;

  /**
   * Enemy Factory for create enemies
   */
 
  std::shared_ptr<EnemyFactory> _enemyFactory;

  /**
   * Time when the wave should activate
   */
  long _startWaveTime;

  /**
   *
   */
  long _aproxEndTime;

  /**
   * IdWave
   */
  std::string  _idWave;

public:
  /**
   * Return a vector of the active enemies.
   *
   */
  inline std::vector<std::shared_ptr<Enemy>>  getActiveEnemies(){
    std::vector<std::shared_ptr<Enemy>> activeEnemies;
    for(uint i=0; i < _enemies.size(); i++){
    	  if(_enemies[i].get()->getActive()){
	    activeEnemies.push_back(_enemies[i]);
	  }
    }
    return activeEnemies;
  }
  Wave(t_waveProperties waveProperties,std::shared_ptr<EnemyFactory> enemyFactory,long startWaveTime);
  /**
   * Update all the enemies of this Wave. The enemies advance according to the speed.
   * @return Number of enemies that have left the map. This number is used to reduce the
   * remaining lives of the player.
   */
  int update(double deltaTime);

  /**
   * Add Enemies to the Wave.
   * @param enemyType Type of the enemy. It must be declared in enemys.cfg
   * @param numberOfEnemies. Number of Enemies to add.
   * @param interval Time in seconds between each enemy appear
   * @param subWaveIdx. Index of the Subwave. Needed to provide unique identifier to the enemy
   * @param nextEnemyTime. Start time of this wave.
   */
  long addEnemies(std::string enemyType, int numberOfEnemies, float interval,
		  int subWaveIdx, long nextEnemyTime);

  /**
   * Check projectiles hits. Hurt and kill the right enemies.
   */
  Reward manageHits(std::vector<std::shared_ptr<Projectile>> projectiles);

  /**
   * Get an aproximated time of when the waves end
   */
  inline long getAproxEndTime(){
    return _aproxEndTime;

  }

  /**
   * Get the time when the Wave should start
   */
  inline long getStartTime(){
    return _startWaveTime;

  }
  /**
   * Return true if the start time of the wave has passed
   */
  inline bool isActive(){
    return (_startWaveTime <= (long)time(NULL));

  }


};


}
#endif

  

#ifndef TOWER_FACTORY_H
#define TOWER_FACTORY_H
#include <map>
#include <string>
#include <memory>
#include "Tower.h"

#define TOWER_CANON "CanonTower"

namespace Tower3d {

/*
 * Factory Pattern Class that loads the towers definition from
 * towers.conf
 */
class TowerFactory {
protected:
  std::map<std::string, std::map<std::string, std::string>> _towerDefs;

public:
  TowerFactory();
  std::unique_ptr<Tower> createTower(std::string towerType, int id,
                                     int level = 0);


  /**
   * Get Tower Price, to check if the player can Pay
   * @param towerType Type of the tower
   * @param leve Level of the tower
   */
  inline float getTowerPrice(string towerType, int level = 0) {
        if (level > 0) {
      stringstream str;
      str << towerType << level;
      towerType = str.str();
    }
    return atof(_towerDefs[towerType].at("price").c_str());
  };

  /**
   * Check if tower can be upgraded
   * Return True
   */
  inline bool towerHasNextLevel(std::shared_ptr<Tower> tower) {
    std::stringstream str;
    str << tower->getType() << (tower->getLevel() + 1);

    return _towerDefs.count(str.str()) > 0;
  };
};
}
#endif

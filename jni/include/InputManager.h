/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef InputManager_H
#define InputManager_H


#include <OgreFrameListener.h>
#include <OgreSingleton.h>
#include <OgreRenderWindow.h>
#include "InputHandler.h"

namespace Tower3d{
  /**
   * Manage all input events
   */
class InputManager : public Ogre::Singleton<InputManager>
                  

{
 public:
  /**
   * Default constructor
   */
  InputManager();

  /**
   * Default destructor
   */
  virtual ~InputManager();

  /**
   * Checks if the are new Input Events.
   */
  void capture();




  /**
   * Add an Input Handler
   * @param instanceName Name of the input handler instance
   * @param inputHandler Input handler
   */
  void addInputHandler(const std::string& instanceName, InputHandler* inputHandler);

  /**
   * remove Input Handler
   * @parma instanceName Instance name of the inputHandler to remove 
   */
  void removeInputHandler(const std::string& instanceName);


  /**
   * remove all inputs handlers.
   */
  void removeAllInputHandlers();



  /**
   * Get singleton
   */
  static InputManager& getSingleton();

  /**
   * Get singleton pointer
   */
  static InputManager* getSingletonPtr();

 private:



  /**
   * List of registered input handlers
   */
  std::map<std::string, InputHandler*> _inputHandlers;



};
}
#endif

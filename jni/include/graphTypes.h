#ifndef GRAPH_TYPES_H
#define GRAPH_TYPES_H
#include <string>
#include <vector>
/**
 * @brief Stores one tileMap properties.
 */
struct tile_props {
  /**
   * @brief  Numeric id of the tile
   */
  std::string id;
  /**
   * @brief position X axis
   */
  float x;
  /**
   * @brief position Y Axis
   */
  float y;
  /**
   * @brief position Z Axis
   */
  float z;
  /**
   * @brief tileType String tile Type (terrain,road,begin,end)
   */
  std::string type;

  /**
   * @brief boost tile id
   */
  int idBoost;
};

/**
 * @brief type of tile props
 */
typedef tile_props tile_props_t;

/**
 * @breif List of tile props
 */
typedef std::vector<tile_props_t> ruta_t;  // Tipo vector de propiedades de un
                                           // tile. Para declarar un vector que
                                           // albergue la ruta elegida de las
                                           // posibles
// devueltas por los algoritmos de búsqueda.

/**
 * @struct way_props
 * Struct that holds de graph ways properties
 */
struct way_props {
  /**
   * Way identifier
   */
  std::string id;
  /**
   * Source NodeId
   */
  int source;
  /**
   * Target Node Id
   */
  int target;
  /**
   * Weight of the way
   */
  int weight;
};

/**
 * Properties graph
 */
struct graph_props 
                   
    {
      /**
       * Id of the graph
       */
  int id;  

      /**
       * Name of the graph
       */

  std::string nombre; 
};

#endif

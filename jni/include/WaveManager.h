#ifndef WAVE_MANAGER_H
#define WAVE_MANAGER_H

#include "Wave.h"
#include <vector>
#include <map>
#include <string>
#include "towerCommon.h"

#define WAVE_INTERVAL 45


namespace Tower3d{
  class Projectile;

  /**
   *  Loads waves info from waves.cfg
   */
class WaveManager {

protected:

  /**
   * list of waves
   */
  std::vector<std::shared_ptr<Wave>> _waves;

  /**
   * factory of enemies. Needed for creating all enemies of the wave
   */
  std::shared_ptr<EnemyFactory> _enemyFactory;

  /**
   * Controls what wave is beeing created
   */
  int _currentWaveIndex;

  /**
   * Time since lastWave
   */
  double _timeSinceLastWave;




public:
  /**
   * Get all enemies in active State
   */
  std::vector<std::shared_ptr<Enemy>> getActiveEnemies();

  /**
   * Constructor. Reads wave.cfg
   * @param enemyFactory. EnemyFactory, needed for create enemies
   */
  WaveManager(std::shared_ptr<EnemyFactory> enemyFactory);

  /**
   * Start all the waves
   */
  void startWaves();
  /**
   * Update all active Waves, including each enemy
   * @param deltaTime. Time elapsed since last update.
   */
  int updateWaves(double deltaTime);
  /**
   * Get number of waves
   */
  inline int getNumWaves(){return _waves.size();};
  /**
   * Manage the hits of a list of projectiles. Destroys and damage enemys.
   * @parama projectiles. List of impacting projectiles
   */
  Reward manageHits(std::vector<std::shared_ptr<Projectile>> projectiles);

  /**
   * Return the number of the lastActive Wave
   */
  inline int getCurrentWave(){
    return _currentWaveIndex;

  }
  /**
   * Return de time to the next Wave
   */
  inline long getSecondsToNextWave(){
    if((_currentWaveIndex+1)< _waves.size()){
      return _waves[_currentWaveIndex+1]->getStartTime() - long(time(NULL));
    }
    return 0;

  }
  bool  isEnded();

  
};
}
#endif

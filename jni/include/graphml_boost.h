#ifndef GRAPHML_BOOST_H
#define GRAPHML_BOOST_H
#define ALL_TYPES "ALL"
#include "graphTypes.h"
#include <fstream>
#include <iostream>
#include <stack>
#include <boost/config.hpp>
#include <boost/graph/graphml.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/random_spanning_tree.hpp>
#include <boost/graph/named_function_params.hpp>
#include <boost/graph/graph_utility.hpp>
#include "OgreVector3.h"

using namespace std;

/**
 * @class graphml_boost
 * Boost implementation of a graph. Used by pathFinding, and tile positioning.
 */
class graphml_boost {
 public:
  /**
   * @struct tile_props
   * Properties of a map tile
   */

  /**
   * Adjacency List ot hte graph
   */
  typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
                                tile_props,  // Tile Properties
                                way_props,  // way properties
                                graph_props

                                > graph_t;

  typedef boost::graph_traits<graph_t>::vertex_descriptor vertex_descriptor_t;
  typedef boost::graph_traits<graph_t>::edge_descriptor edge_descriptor_t;

  ruta_t getVertices(std::string type);
  inline tile_props getGraphNode(int idNode) { return _graph[idNode]; };
  void calculateRouteBFS(int idSource);
  void loadGraph(std::istream* inputStream);
  inline graph_t getGraph() { return _graph; };
  ruta_t getRuta(size_t idSource, size_t idDestiny);
  tile_props getCurrentNode(Ogre::Vector3 position);
  ~graphml_boost();

 private:
  std::ifstream _ifstream;  // manejador del fichero.
  boost::dynamic_properties _dp;
  graph_t _graph;
  std::vector<vertex_descriptor_t> _p;
};

#endif  // GRAPHML_BOOST_H

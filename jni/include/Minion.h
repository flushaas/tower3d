#include <string>

namespace Tower3d {

/**
 * @Class Minion
 * @brief Represent a minion. The unique minion task is to attack the towers.
 */
class Minion {
  /**
   * UniqueId of the minion. Format "minion_XXX".
   */
  string _id;

  /**
   * Speed of the minion
   */
  float _speed;

  /**
   * Life points of the minion
   */
  float _life;

 public:
  /**
   *  return the Id
   */
  inline float getId() { return _id; };

  /**
   * return the speed of the minion.
   */
  inline float getSpeed() { return _speed; };

  /**
   * return the lifePoints of the minion
   */
  inline float getLife() { return _life; };
}
}

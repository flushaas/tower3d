# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#LOCAL_PATH := $(call my-dir)

#include $(CLEAR_VARS)

#LOCAL_MODULE    := hello-jni
#LOCAL_SRC_FILES := hello-jni.c



include $(call all-subdir-makefiles)	
LOCAL_PATH := $(call my-dir)
#SDL_PATH := ../SDL
#LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include
    include $(CLEAR_VARS)
    LOCAL_MODULE    := Tower3d
    LOCAL_LDLIBS	:= -landroid -lc -lm -ldl -llog -lEGL -lGLESv2
    LOCAL_LDLIBS	+= -L/home/flush/PFC/ogre2/ogre/build/lib -L/home/flush/PFC/ogredeps/build/lib/armeabi-v7a -L/home/flush/PFC/ 
    LOCAL_LDLIBS	+= -lPlugin_ParticleFXStatic -lPlugin_OctreeSceneManagerStatic -lRenderSystem_GLES2Static -lOgreRTShaderSystemStatic -lOgreOverlayStatic -lOgreTerrainStatic -lOgrePagingStatic -lOgreVolumeStatic -lOgreMainStatic 
    LOCAL_LDLIBS	+= -lzziplib -lz -lFreeImage -lfreetype -lOIS  /home/flush/PFC/ogre2/ogre/build/systemlibs/armeabi-v7a/libsupc++.a /home/flush/PFC/ogre2/ogre/build/systemlibs/armeabi-v7a/libstdc++.a /home/flush/PFC/ogre2/ogre/build/SampleBrowserNDK/obj/local/armeabi-v7a/libcpufeatures.a /home/flush/PFC/boost_1_61_0/stage/lib/armabi-v7a/libboost_graph.a /home/flush/PFC/boost_1_61_0/stage/lib/armabi-v7a/libboost_iostreams.a
    LOCAL_STATIC_LIBRARIES := android_native_app_glue cpufeatures
    LOCAL_CFLAGS := -DGL_GLEXT_PROTOTYPES=1 -I/home/flush/PFC/ogre2/ogre/build/include -I/home/flush/PFC/ogre2/ogre/OgreMain/include -I/home/flush/PFC/ogre2/ogre/RenderSystems/GLES2/include -I/home/flush/PFC/ogre2/ogre/RenderSystems/GLES2/include/EGL
    LOCAL_CFLAGS += -I/home/flush/PFC/android/android-ndk-r10e/sources/cpufeatures -I/home/flush/PFC/ogre2/ogre/Components/RTShaderSystem/include -I/home/flush/PFC/ogre2/ogre/Components/Overlay/include -I/home/flush/PFC/ogre2/ogre/Components/Volume/include -I/home/flush/PFC/ogre2/ogre/Components/Terrain/include -I/home/flush/PFC/ogre2/ogre/Components/Paging/include
    LOCAL_CFLAGS += -I/home/flush/PFC/ogre2/ogre/PlugIns/ParticleFX/include -I/home/flush/PFC/ogre2/ogre/PlugIns/OctreeSceneManager/include 
    LOCAL_CFLAGS += -I/home/flush/PFC/ogredeps/build/include -I/home/flush/PFC/ogredeps/src/ois/includes 
    LOCAL_CFLAGS += -I/home/flush/PFC/android/android-ndk-r10e/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86/user/include -I/home/flush/PFC/TOWER3D/jni/include
    LOCAL_CFLAGS += -I/home/flush/PFC/boost_1_61_0/ -I/home/flush/PFC/SDL/SDL/include -I/home/flush/PFC/SDL/SDL2_mixer -I/home/flush/PFC/SDL/SDL2_image -I/home/flush/PFC/ogre2/ogre/PlugIns/ParticleFX/include


    LOCAL_CFLAGS += -fexceptions -frtti  -D___ANDROID___ -DANDROID -DZZIP_OMIT_CONFIG_H -DINCLUDE_RTSHADER_SYSTEM=1
    LOCAL_PATH := /home/flush/PFC/TOWER3D/jni/src
    FILE_LIST := $(wildcard $(LOCAL_PATH)/*.cpp) 
    LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%) \
	$(LOCAL_PATH)/SDL_android_main.c



    LOCAL_SHARED_LIBRARIES := SDL2 SDL2_mixer SDL2_image
    include $(BUILD_SHARED_LIBRARY) 
    $(call import-module,android/cpufeatures) 
    $(call import-module,android/native_app_glue)




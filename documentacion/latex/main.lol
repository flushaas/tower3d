\contentsline {lstlisting}{\numberline {5.1}Comando para averiguar si el sistema es Big Endian o Little Endian en Linux.}{46}{lstlisting.5.1}
\contentsline {lstlisting}{\numberline {5.2}L\IeC {\'\i }neas a sustituir en CMakeList.txt}{46}{lstlisting.5.2}
\contentsline {lstlisting}{\numberline {5.3}C\IeC {\'o}digo para establecer Little Endian en CMakeList.txt. Cambiar FALSE por TRUE si el sistema es Big Endian }{47}{lstlisting.5.3}
\contentsline {lstlisting}{\numberline {5.4}Corregir los paths de est\IeC {\'a}s l\IeC {\'\i }neas. Solamente ser\IeC {\'a} necesario cambiar el n\IeC {\'u}mero de versi\IeC {\'o}n (en este ejemplo 4.9) }{47}{lstlisting.5.4}
\contentsline {lstlisting}{\numberline {5.5}Comando para compilar Ogre para Android. Los 4 \IeC {\'u}ltimos flags no figuran en \cite {ogre_android}}{47}{lstlisting.5.5}
\contentsline {lstlisting}{\numberline {5.6}Ejemplo del contenido de enemys.cfg}{50}{lstlisting.5.6}
\contentsline {lstlisting}{\numberline {5.7}Ejemplo del contenido de waves.cfg}{52}{lstlisting.5.7}
\contentsline {lstlisting}{\numberline {5.8}Ejemplo del contenido de towers.cfg}{55}{lstlisting.5.8}
\contentsline {lstlisting}{\numberline {5.9}Un archivo graphML}{58}{lstlisting.5.9}
\contentsline {lstlisting}{\numberline {5.10}C\IeC {\'o}digo para lanzar una b\IeC {\'u}squeda por anchura.}{59}{lstlisting.5.10}
\contentsline {lstlisting}{\numberline {5.11}Script de overlays para dibujar los botones del men\IeC {\'u}.}{70}{lstlisting.5.11}
\contentsline {lstlisting}{\numberline {5.12}Carga de la pantalla de Loading. Se aprecia la diferencia de c\IeC {\'o}digo seg\IeC {\'u}n la plataforma.}{72}{lstlisting.5.12}
\contentsline {lstlisting}{\numberline {6.1}Ejemplo de bifurcaci\IeC {\'o}n de c\IeC {\'o}digo mediante el uso de OGRE\_PLAFORM.}{84}{lstlisting.6.1}

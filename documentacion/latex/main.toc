\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {xiii}}{chapter*.1}
\contentsline {chapter}{\'{I}ndice general}{\es@scroman {xv}}{chapter*.2}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {xix}}{chapter*.4}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xxi}}{chapter*.5}
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xxv}}{chapter*.8}
\contentsline {chapter}{Listado de acr\IeC {\'o}nimos}{\es@scroman {xxvii}}{chapter*.9}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Justificaci\IeC {\'o}n}{2}{section.1.1}
\contentsline {subsubsection}{\IeC {\textquestiondown }Por qu\IeC {\'e} Software Libre?}{2}{section.1.1}
\contentsline {subsubsection}{Plataforma objetivo principal}{3}{section.1.1}
\contentsline {subsubsection}{Enfoque}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Estructura del documento}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Capitulo 1: Introducci\IeC {\'o}n}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Cap\IeC {\'\i }tulo 2: Objetivos}{4}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Cap\IeC {\'\i }tulo 3: Antecedentes}{4}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Cap\IeC {\'\i }tulo 4: M\IeC {\'e}todo de trabajo}{4}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Cap\IeC {\'\i }tulo 5: Resultado Final}{4}{subsection.1.2.5}
\contentsline {subsection}{\numberline {1.2.6}Conclusiones y propuestas}{4}{subsection.1.2.6}
\contentsline {subsection}{\numberline {1.2.7}Bibliograf\IeC {\'\i }a}{4}{subsection.1.2.7}
\contentsline {subsection}{\numberline {1.2.8}Ap\IeC {\'e}ndices}{5}{subsection.1.2.8}
\contentsline {chapter}{\numberline {2}Antecedentes}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}La industria del videojuego}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Aumento de usuarios}{7}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}G\IeC {\'e}neros de videojuegos}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Lucha}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Arcade}{10}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Disparos}{11}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Carreras}{13}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Estrategia}{14}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}\ac {MMO}}{16}{subsection.2.2.6}
\contentsline {section}{\numberline {2.3}Dise\IeC {\~n}o de un videojuego}{17}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Las emociones como parte del videojuego}{17}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Equipo de desarrollo de un videojuego}{18}{subsection.2.3.2}
\contentsline {subsubsection}{Dise\IeC {\~n}ador}{19}{subsection.2.3.2}
\contentsline {subsubsection}{Programador}{19}{subsection.2.3.2}
\contentsline {subsubsection}{Equipo art\IeC {\'\i }stico de sonido}{19}{subsection.2.3.2}
\contentsline {subsubsection}{Artistas}{20}{subsection.2.3.2}
\contentsline {subsubsection}{Productor}{21}{subsection.2.3.2}
\contentsline {subsubsection}{Betatester}{21}{subsection.2.3.2}
\contentsline {subsubsection}{Playtester}{21}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Motores de juego}{21}{subsection.2.3.3}
\contentsline {subsubsection}{Unreal Engine 4}{22}{subsection.2.3.3}
\contentsline {subsubsection}{Unity}{23}{figure.2.13}
\contentsline {subsubsection}{Ogre3D}{24}{figure.2.14}
\contentsline {chapter}{\numberline {3}Objetivos}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Objetivo general}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}Objetivos espec\IeC {\'\i }ficos}{25}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Objetivos funcionales o de dise\IeC {\~n}o}{25}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Objetivos t\IeC {\'e}cnicos o de implementaci\IeC {\'o}n}{26}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Objetivos Comerciales}{27}{subsection.3.2.3}
\contentsline {chapter}{\numberline {4}M\IeC {\'e}todo de trabajo}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodolog\IeC {\'\i }a}{29}{section.4.1}
\contentsline {section}{\numberline {4.2}Hardware}{30}{section.4.2}
\contentsline {section}{\numberline {4.3}Motor de Juego}{30}{section.4.3}
\contentsline {subsubsection}{GamePlay3D}{31}{section.4.3}
\contentsline {subsubsection}{Blend4Web}{31}{figure.4.1}
\contentsline {subsubsection}{Dim3}{32}{figure.4.2}
\contentsline {subsubsection}{Godot}{32}{figure.4.3}
\contentsline {subsubsection}{Ogre3D}{33}{figure.4.5}
\contentsline {section}{\numberline {4.4}Otras herramientas de Software}{35}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Sistema Operativo}{35}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Emacs}{35}{subsection.4.4.2}
\contentsline {subsubsection}{\ac {GCC}}{37}{figure.4.7}
\contentsline {subsubsection}{\ac {GIMP}}{38}{figure.4.7}
\contentsline {subsubsection}{Blender}{39}{figure.4.8}
\contentsline {subsubsection}{Android \ac {NDK} y \ac {SDK}}{39}{figure.4.9}
\contentsline {chapter}{\numberline {5}Resultados}{41}{chapter.5}
\contentsline {section}{\numberline {5.1}Dise\IeC {\~n}o del videojuego}{41}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Desarrollo del concepto}{41}{subsection.5.1.1}
\contentsline {paragraph}{El concepto b\IeC {\'a}sico}{41}{subsection.5.1.1}
\contentsline {paragraph}{La propuesta del juego}{41}{subsection.5.1.1}
\contentsline {paragraph}{GamePlay}{42}{figure.5.1}
\contentsline {section}{\numberline {5.2}Preparando el entorno}{44}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Instalaci\IeC {\'o}n de Android NDK y SDK}{44}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Compilando Ogre3D para Android}{44}{subsection.5.2.2}
\contentsline {subsubsection}{Descarga del c\IeC {\'o}digo fuente}{45}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Descarga y compilaci\IeC {\'o}n de las dependencias de OGRE3D}{45}{subsection.5.2.3}
\contentsline {subsubsection}{Compilando Ogre3D}{45}{subsection.5.2.3}
\contentsline {paragraph}{Problema con la detecci\IeC {\'o}n del Endianess}{46}{subsection.5.2.3}
\contentsline {paragraph}{Errores al buscar el directorio de las librer\IeC {\'\i }as de C++}{46}{lstnumber.5.3.1}
\contentsline {paragraph}{Errores FreeType y PKGConfig}{47}{lstnumber.5.4.5}
\contentsline {paragraph}{Errores en la compilacion de SampleBrowser}{47}{lstnumber.5.5.7}
\contentsline {section}{\numberline {5.3}Arquitectura del proyecto}{48}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Gesti\IeC {\'o}n de Enemigos}{48}{subsection.5.3.1}
\contentsline {subsubsection}{Enemy}{48}{figure.5.5}
\contentsline {subsubsection}{EnemyFactory}{50}{figure.5.5}
\contentsline {subsubsection}{Wave}{50}{lstnumber.5.6.5}
\contentsline {subsubsection}{WaveManager}{51}{lstnumber.5.6.5}
\contentsline {subsection}{\numberline {5.3.2}Gesti\IeC {\'o}n de las torres}{52}{subsection.5.3.2}
\contentsline {subsubsection}{Projectile}{53}{figure.5.6}
\contentsline {subsubsection}{Tower}{53}{figure.5.6}
\contentsline {subsubsection}{TowerFactory}{54}{figure.5.6}
\contentsline {subsection}{\numberline {5.3.3}Gesti\IeC {\'o}n del mapa}{56}{subsection.5.3.3}
\contentsline {subsubsection}{MapTile}{56}{figure.5.7}
\contentsline {subsubsection}{graphmlBoost}{56}{figure.5.7}
\contentsline {subsubsection}{Map}{60}{lstnumber.5.10.8}
\contentsline {subsubsection}{MapManager}{60}{lstnumber.5.10.8}
\contentsline {subsection}{\numberline {5.3.4}Sistemas Transversales o de Apoyo}{61}{subsection.5.3.4}
\contentsline {subsubsection}{Sistema de Sonido}{61}{subsection.5.3.4}
\contentsline {paragraph}{SoundManager }{61}{subsection.5.3.4}
\contentsline {paragraph}{Sound}{61}{subsection.5.3.4}
\contentsline {subsubsection}{Control de Flujo del juego}{62}{subsection.5.3.4}
\contentsline {section}{\numberline {5.4}Decisiones T\IeC {\'e}cnicas}{63}{section.5.4}
\contentsline {subsubsection}{Edici\IeC {\'o}n de Mapas}{63}{section.5.4}
\contentsline {subsubsection}{Dibujado del mapa}{65}{figure.5.10}
\contentsline {subsection}{\numberline {5.4.1}Gesti\IeC {\'o}n de la entrada}{66}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Interfaz de usuario}{66}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}T\IeC {\'e}cnicas de Sombreado}{67}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Ogre3D y SDL}{69}{subsection.5.4.4}
\contentsline {subsubsection}{Ogre3D crea la ventana}{69}{subsection.5.4.4}
\contentsline {subsubsection}{SDL crea la ventana}{70}{subsection.5.4.4}
\contentsline {subsection}{\numberline {5.4.5}Otros ajustes}{71}{subsection.5.4.5}
\contentsline {subsubsection}{Problemas con las rutas}{71}{subsection.5.4.5}
\contentsline {subsubsection}{Conflictos entre SDL\_Image y Ogre3D}{72}{lstnumber.5.12.15}
\contentsline {subsubsection}{Carga autom\IeC {\'a}tica de Plugins}{72}{lstnumber.5.12.15}
\contentsline {section}{\numberline {5.5}An\IeC {\'a}lisis de Costes}{72}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Investigaci\IeC {\'o}n y establecimiento de entorno de desarrollo.}{73}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Desarrollo del prototipo}{73}{subsection.5.5.2}
\contentsline {section}{\numberline {5.6}Monetizaci\IeC {\'o}n y Marketing}{73}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Definici\IeC {\'o}n del producto y mercado objetivo}{74}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Modelos de negocio}{74}{subsection.5.6.2}
\contentsline {subsection}{\numberline {5.6.3}Calculo de amortizaci\IeC {\'o}n}{77}{subsection.5.6.3}
\contentsline {subsection}{\numberline {5.6.4}Marketing}{78}{subsection.5.6.4}
\contentsline {chapter}{\numberline {6}Conclusiones y propuestas}{81}{chapter.6}
\contentsline {section}{\numberline {6.1}Conclusiones}{81}{section.6.1}
\contentsline {section}{\numberline {6.2}Objetivos de dise\IeC {\~n}o}{81}{section.6.2}
\contentsline {section}{\numberline {6.3}Objetivos t\IeC {\'e}cnicos o de implementaci\IeC {\'o}n}{83}{section.6.3}
\contentsline {section}{\numberline {6.4}Conclusiones globales}{87}{section.6.4}
\contentsline {section}{\numberline {6.5}Propuestas de trabajo futuro}{90}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}Mejoras en la calidad gr\IeC {\'a}fica}{90}{subsection.6.5.1}
\contentsline {subsubsection}{Multijugador}{93}{figure.6.8}
\contentsline {chapter}{\numberline {A}Guia r\IeC {\'a}pida de uso}{101}{appendix.Alph1}
\contentsline {subsection}{\numberline {A.0.1}Pantalla de cargando}{101}{subsection.Alph1.0.1}
\contentsline {subsection}{\numberline {A.0.2}Pantalla de Men\IeC {\'u}}{101}{subsection.Alph1.0.2}
\contentsline {subsection}{\numberline {A.0.3}Pantalla Principal de juego}{101}{subsection.Alph1.0.3}
\contentsline {paragraph}{Construcci\IeC {\'o}n de una torre.}{102}{figure.Alph1.3}
\contentsline {paragraph}{Mejora de las torres.}{102}{figure.Alph1.6}
\contentsline {chapter}{\numberline {B}Estructura F\IeC {\'\i }sica del proyecto}{107}{appendix.Alph2}
\contentsline {chapter}{Bibliograf\'{\i }a}{109}{appendix*.35}

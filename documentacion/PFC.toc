\select@language {spanish}
\contentsline {paragraph}{RESUMEN}{1}
\contentsline {section}{\numberline {1}INTRODUCCI\'ON}{2}
\contentsline {section}{\numberline {2}OBJETIVOS DEL PFC}{3}
\contentsline {subsection}{\numberline {2.1}Entorno de trabajo}{3}
\contentsline {subsubsection}{\numberline {2.1.1}GIMP}{3}
\contentsline {subsubsection}{\numberline {2.1.2}Blender}{4}
\contentsline {subsubsection}{\numberline {2.1.3}EMACS}{5}
\contentsline {subsubsection}{\numberline {2.1.4}GCC}{6}
\contentsline {subsection}{\numberline {2.2}Objetivos t\'ecnicos}{6}
\contentsline {subsection}{\numberline {2.3}Objetivos funcionales}{7}
\contentsline {section}{\numberline {3}ANTECEDENTES}{8}
\contentsline {subsection}{\numberline {3.1}La industria del videojuego.}{8}
\contentsline {subsection}{\numberline {3.2}Dise\~no de un videojuego.}{8}
\contentsline {subsubsection}{\numberline {3.2.1}Las emociones como parte del videojuego.}{8}
\contentsline {subsection}{\numberline {3.3}Motores de juego.}{9}
\contentsline {subsubsection}{\numberline {3.3.1}Unreal Engine 4.}{9}
\contentsline {subsubsection}{\numberline {3.3.2}Unity}{10}
\contentsline {section}{\numberline {4}M\'ETODO DE TRABAJO}{12}
\contentsline {section}{\numberline {5}ARQUITECTURA}{12}
\contentsline {subsection}{\numberline {5.1}Entorno de desarrollo.}{12}
\contentsline {subsubsection}{\numberline {5.1.1}Editor}{13}
\contentsline {subsubsection}{\numberline {5.1.2}Android NDK y SDK}{13}
\contentsline {subsection}{\numberline {5.2}Estructura F\IeC {\'\i }sica.}{13}
\contentsline {subsection}{\numberline {5.3}Estructura L\'ogica}{14}
\contentsline {subsubsection}{\numberline {5.3.1}Gesti\'on de Enemigos}{14}
\contentsline {subsubsection}{\numberline {5.3.2}Gesti\'on de las torres}{16}
\contentsline {subsubsection}{\numberline {5.3.3}Gesti\'on del mapa.}{18}
\contentsline {subsubsection}{\numberline {5.3.4}Gesti\'on de sistemas de Apoyo o Transversales.}{20}
\contentsline {section}{\numberline {6}DECISIONES T\'ECNICAS}{20}
\contentsline {subsection}{\numberline {6.1}Dise\~no de mapas.}{20}
\contentsline {subsection}{\numberline {6.2}Dibujado del mapa.}{22}
\contentsline {subsection}{\numberline {6.3}Gesti\'on de la entrada.}{23}
\contentsline {subsection}{\numberline {6.4}Interfaz de usuario.}{23}
\contentsline {subsection}{\numberline {6.5}T\'ecnica de Sombreado.}{24}
\contentsline {section}{\numberline {7}AN\'ALISIS DE COSTES}{24}
\contentsline {subsection}{\numberline {7.1}Investigaci\'on y establecimiento de entorno de desarrollo.}{24}
\contentsline {subsection}{\numberline {7.2}Desarrollo del videojuego}{25}
\contentsline {section}{\numberline {8}MANUAL DE USUARIO}{26}
\contentsline {subsection}{\numberline {8.1}Pantalla de cargando}{26}
\contentsline {subsection}{\numberline {8.2}Pantalla de Men\'u}{26}
\contentsline {subsection}{\numberline {8.3}Pantalla Principal de juego}{27}
\contentsline {paragraph}{Construcci\'on de una torre.}{28}
\contentsline {paragraph}{Mejora de las torres.}{29}
\contentsline {section}{\numberline {9}CONCLUSIONES Y PROPUESTAS}{30}
\contentsline {subsection}{\numberline {9.1}Conclusiones}{30}
\contentsline {subsection}{\numberline {9.2}Propuestas de trabajo futuro}{31}
\contentsline {section}{\numberline {10}BIBLIOGRAFIA}{32}
